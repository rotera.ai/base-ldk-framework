/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import EventEmitter from 'events';
import { whisper } from '@oliveai/ldk';
import { LoopEvents } from './utils';

/**
 * EventBus
 *
 * @example
 * ```@js
 * import {EventBus} from './eventBus`
 *
 * // Global event bus
 * EventBus.emit('event-name', 'argument');
 * EventBus.on('event-name', (param) => {});
 *
 * // Component event bus
 * this.emit('event-name', 'argument');
 * <component>.on('event-name', (param) => {});
 * ```
 */
export const EventBus: NodeJS.EventEmitter = new EventEmitter();

/**
 * EventType
 *
 * A base to include all the event types emitted from this framework.
 * If a loop needs additional events then an additional enum can be created and
 * merged with a const.
 *
 * @example
 * ```@js
 * import {EventType as BaseEventType} from '@rotera.ai/base-ldk-framework/eventBus';
 *
 * enum LoopEventType = { EventName = 'event-name'};
 * const EventType = {...BaseEventType, ...LoopEventType};
 * ```
 */
export enum EventType {
  Click = 'click',
  Blur = 'blur',
  Focus = 'focus',
  Change = 'change',
  Select = 'select',
}

/**
 * Events
 *
 * TODO The `LoopEvents` parameter is confusing, but I'm having issues passing
 *  `this` with the component type as `WrappedLDKComponent` due to the lifecycle of the component.
 */
export const events = {
  onClick: (component: LoopEvents) => (error: Error | undefined, whisper: whisper.Whisper, value?: boolean) => {
    if (!error) {
      component.emit(EventType.Click, whisper, value);
    } else {
      throw error;
    }
  },
  onChangeWithValue: (component: LoopEvents) => (
    error: Error | undefined,
    value: string | boolean | number,
    whisper: whisper.Whisper
  ) => {
    if (!error) {
      component.emit(EventType.Change, value, whisper);
    } else {
      throw error;
    }
  },
  onBlur: (component: LoopEvents) => (error: Error | undefined) => {
    if (!error) {
      component.emit(EventType.Blur);
    } else {
      throw error;
    }
  },
  onFocus: (component: LoopEvents) => (error: Error | undefined) => {
    if (!error) {
      component.emit(EventType.Focus);
    } else {
      throw error;
    }
  },
  onSelectWithValue: (component: LoopEvents) => (
    error: Error | undefined,
    value: string | boolean | number,
    whisper: whisper.Whisper
  ) => {
    if (!error) {
      component.emit(EventType.Select, value, whisper);
    } else {
      throw error;
    }
  },
};
