/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import { whisper } from '@oliveai/ldk';
import { EventBus } from './eventBus';

/**
 * LDK Component
 *
 * A type wrapped around the LDK `Component` to give it a more meaningful name in our package.
 */
export type LDKComponent = whisper.Component;

/**
 * Render Components
 *
 * Renders array of components to corresponding `Component`
 * type in `@oliveai/ldk/src/whisper`.
 *
 * @example ```@js
 * await whisper.create({
 *    label: 'abc',
 *    components: renderComponents(<component>, <component>),
 * });
 * ```
 */
export function renderComponents<C = LDKComponent>(...args: any): C[] {
  const components = [];

  for (const component of args) {
    // If `component` is not of type `Component`, call `render()`
    const renderedComponent = component.render ? component.render() : component;

    if (Array.isArray(renderedComponent)) {
      components.push(...renderedComponent);
    } else {
      components.push(renderedComponent);
    }
  }

  return components;
}

/**
 * Use State
 *
 * A hook to set the state of a component
 *
 * Note that this method is NOT currently used in our whisper/component creation methodology. It was part of
 * another direction, more like React hooks. Ultimately, we went a different direction, but maybe
 * this could be used down the road for something or give an engineer an idea of how to handle some
 * type of state in the future.
 *
 * @example ```@js
 * type State = {
 *     a: string;
 *     b: Record<string, number>
 * }
 * const [state, setState] = useState<State>({
 *   a: '123',
 *   b: {a: 1, b: 2, c: 3},
 * });
 * ```

 export function useState<S>(obj: S): [value: S, setState: Function] {
   const state = obj;

   function setValue(newState: S): void {
     Object.assign(state, newState);
   }
   return [state, setValue];
 }
*/

/**
 * Methods
 *
 * The base methods in a whisper or component.
 * Adding additional methods is ok and even recommended to keep the code
 * easy to read, but these are the most common.
 */
type Methods = {
  // Where all components should be declared
  getComponents: (...args: any) => WhisperComponent[];
  // Where all component event listeners should live
  addEventListeners?: () => void;
  // Where all component event listener should be removed to avoid memory leaks
  removeEventListeners?: () => void;
  // Where API requests and component data declarations should live
  getData?: Function; //() => Promise<any> | void;
};

/**
 * LifeCycle
 *
 * The base methods in a whisper or component.
 */
type BaseLifeCycle = {
  // Where all code goes to remove memory leaks and functionality.
  // Should be called from `whisper.onClose()`
  destroy?: () => void;
};

type ComponentLifeCycle = BaseLifeCycle & {
  // Renders all components from `getComponents()` to a `LDKComponent` type
  render: () => LDKComponent[];
};

type WhisperLifeCycle = BaseLifeCycle & {
  // Renders the whisper.
  // Note that this method is automatically called and in most cases will never need
  // to be called from a whisper.
  render: () => void;
  // Base method for updating the whisper
  update?: () => void;
};

/**
 * Loop Events
 *
 * The event mechanism for components.
 */
export type LoopEvents = {
  on: typeof EventBus.on;
  emit: typeof EventBus.emit;
  off: typeof EventBus.off;
};

/**
 * Wrapped Component
 *
 * The type used for our custom wrapped components and for `ComponentRegistration` so
 * component properties are exposed in parent components and whispers.
 * (e.g., <component>.on('event', <method>))
 */
export type WrappedLDKComponent = ComponentLifeCycle & LoopEvents & { type: string };

/**
 * Whisper Component
 *
 * The return type for `getComponents`. Since `WhisperComponent.render()` is called in
 * `LifeCycle.render()` not all components are fully rendered within `getComponents()`
 * so the array of components could be a LDK component or a custom wrapped component.
 *
 * @example
 * ```@js
 * [
 *    // Wrapped component
 *    Link({text: 'abc'}),
 *    // LDK component
 *    {
 *       body: 'a',
 *       type: whisper.WhisperComponentType.Markdown,
 *    }
 * ]
 * ```
 */
export type WhisperComponent = WrappedLDKComponent | LDKComponent;

/**
 * Whisper Descriptor
 *
 * The type that passes `this` to the object properties
 */
type WhisperDescriptor<P, C, D, M, LC> = {
  props?: P;
  data?: D;
  components?: C;
  methods?: M & Methods & ThisType<P & C & D & M & LC>;
  lifeCycle: LC & WhisperLifeCycle & ThisType<P & C & D & M & LC>;
};

/**
 * Component Descriptor
 *
 * The type that passes `this` to the object properties
 */
type ComponentDescriptor<P, D, M, C, LC, E, T> = {
  type: T;
  props?: P;
  data?: D;
  components?: C;
  methods: M & Methods & ThisType<P & D & M & C & E>;
  lifeCycle: LC & ComponentLifeCycle & ThisType<P & D & M & C & E>;
};

/**
 * Loop
 *
 * TODO(Cameron/Keith): If a whisper ever becomes stateful we'll need to adjust the return type of `createWhisper`
 *
 * A methodology to help create whispers and components consistently
 */
export const Loop = {
  createWhisper: <P, C, D, M, LC>(desc: WhisperDescriptor<P, C, D, M, LC>): void => {
    const props: object = desc.props || {};
    const data: object = desc.data || {};
    const components: object = desc.components || {};
    const methods: object = desc.methods || {};
    const lifeCycle: WhisperLifeCycle = desc.lifeCycle;
    const whisper = {
      ...props,
      ...data,
      ...components,
      ...methods,
      ...lifeCycle,
    };

    return whisper.render();
  },
  createComponent: <P, D, C, M, LC, T>(
    desc: ComponentDescriptor<P, D, C, M, LC, LoopEvents, T>
  ): WrappedLDKComponent => {
    const props: object = desc.props || {};
    const data: object = desc.data || {};
    const components: object = desc.components || {};
    const methods: object = desc.methods || {};
    const lifeCycle: object = desc.lifeCycle;

    return {
      ...props,
      ...data,
      ...components,
      ...methods,
      ...lifeCycle,
      emit: EventBus.emit,
      on: EventBus.on,
      off: EventBus.off,
      type: desc.type || '',
    } as WrappedLDKComponent;
  },
};

/**
 * Checks if a given object has a property
 * used for typescript typechecking
 */
export function hasOwnProperty<X extends {}, Y extends PropertyKey>(
  obj: X,
  prop: Y
): obj is X & Record<Y, unknown> {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}