/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../utils';
import {events} from '../../eventBus';
import {WrappedWhisperComponentType} from '../../types';

type ButtonProps = {
  label: string;
  buttonStyle?: whisper.ButtonStyle;
  size?: whisper.ButtonSize;
  tooltip?: string;
};

const buttonDefaultProps = {
  buttonStyle: whisper.ButtonStyle.Primary,
  size: whisper.ButtonSize.Large,
  tooltip: '',
};

export function Button(props: ButtonProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.Button,
    props: {...buttonDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            label: this.label,
            buttonStyle: this.buttonStyle,
            size: this.size,
            tooltip: this.tooltip,
            onClick: events.onClick(this),
            type: whisper.WhisperComponentType.Button,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type Button = WrappedLDKComponent;
