/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../utils';
import {WrappedWhisperComponentType} from '../../types';

type ListPairProps = {
  label: string;
  value: string;
  copyable?: boolean;
  labelCopyable?: boolean;
  style?: whisper.Urgency;
};

const listPairDefaultProps = {
  copyable: false,
  labelCopyable: false,
  style: whisper.Urgency.None,
};

export function ListPair(props: ListPairProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.ListPair,
    props: {...listPairDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            label: this.label,
            value: this.value,
            copyable: this.copyable,
            labelCopyable: this.labelCopyable,
            style: this.style,
            type: whisper.WhisperComponentType.ListPair,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type ListPair = WrappedLDKComponent;
