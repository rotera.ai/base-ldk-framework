/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import { whisper } from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../utils';
import { events } from '../../eventBus';
import { WrappedWhisperComponentType } from '../../types';

/**
 * LDKLink
 *
 * A type wrapped around the LDK `Link` to give it a more meaningful name in our package.
 */
export type LDKLink = whisper.Link;

type LinkProps = {
  href?: string;
  text: string;
  style?: whisper.Urgency;
  textAlign?: whisper.TextAlign;
};

const linkDefaultProps = {
  textAlign: whisper.TextAlign.Left,
  style: whisper.Urgency.None,
};

export function Link(props: LinkProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.Link,
    props: { ...linkDefaultProps, ...props },
    methods: {
      getComponents(): WhisperComponent[] {
        // Default Link properties
        const link: LDKLink = {
          text: this.text,
          style: this.style,
          textAlign: this.textAlign,
          type: whisper.WhisperComponentType.Link,
        };
        // Add `href` or `onClick`
        // Note that if both are set, `onClick` takes precedence, so we'll remove this confusion and only set one.
        if (!this.href) {
          link.onClick = events.onClick(this);
        } else {
          link.href = this.href;
        }

        return [link];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type Link = WrappedLDKComponent;
