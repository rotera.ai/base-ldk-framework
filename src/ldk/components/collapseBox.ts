/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import { whisper } from '@oliveai/ldk';
import {
    Loop,
    renderComponents,
    WrappedLDKComponent,
    WhisperComponent,
    LDKComponent,
} from '../../utils';
import { events } from '../../eventBus';
import { WrappedWhisperComponentType } from '../../types';

type CollaposeBoxProps = {
    children: Array<WhisperComponent>;
    label?: string;
    open: boolean;
};

const CollapseBoxDefaultProps = {
    label: '',
};

export function CollapseBox(props: CollaposeBoxProps): WrappedLDKComponent {
    return Loop.createComponent({
        type: WrappedWhisperComponentType.CollapseBox,
        props: { ...CollapseBoxDefaultProps, ...props },
        methods: {
            getComponents(): WhisperComponent[] {
                const clickHandler = events.onClick(this);
                return [
                    {
                        children: renderComponents(...this.children) as whisper.ChildComponents[],
                        label: this.label,
                        onClick: (error, param, whisper) => clickHandler(error, whisper, param),
                        open: this.open,
                        type: whisper.WhisperComponentType.CollapseBox,

                    }
                ];
            },
        },
        lifeCycle: {
            render(): LDKComponent[] {
                return renderComponents(...this.getComponents());
            },
        },
    });
}

export type CollapseBox = WrappedLDKComponent;