import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../../utils';
import {events} from '../../../eventBus';
import {WrappedWhisperComponentType} from '../../../types';

export type EmailFormFieldProps = {
  label: string;
  value?: string;
  tooltip?: string;
};

export const emailFormFieldDefaultProps = {
  value: '',
  tooltip: '',
};

export function EmailFormField(props: EmailFormFieldProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.EmailFormField,
    props: {...emailFormFieldDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            label: this.label,
            value: this.value,
            tooltip: this.tooltip,
            onChange: events.onChangeWithValue(this),
            onBlur: events.onBlur(this),
            onFocus: events.onFocus(this),
            type: whisper.WhisperComponentType.Email,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type EmailFormField = WrappedLDKComponent;
