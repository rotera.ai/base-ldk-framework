import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../../utils';
import {events} from '../../../eventBus';
import {WrappedWhisperComponentType} from '../../../types';

type SelectFormFieldProps = {
  label: string;
  options: string[];
  selected?: number;
  tooltip?: string;
};

const selectFormFieldDefaultProps = {
  tooltip: '',
  selected: -1,
};

export function SelectFormField(props: SelectFormFieldProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.SelectFormField,
    props: {...selectFormFieldDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            label: this.label,
            options: this.options,
            tooltip: this.tooltip,
            selected: this.selected,
            onSelect: events.onSelectWithValue(this),
            type: whisper.WhisperComponentType.Select,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type SelectFormField = WrappedLDKComponent;
