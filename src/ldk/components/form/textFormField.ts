import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../../utils';
import {events} from '../../../eventBus';
import {WrappedWhisperComponentType} from '../../../types';

type TextFormFieldProps = {
  label: string;
  value?: string;
  tooltip?: string;
};

const textFormFieldDefaultProps = {
  value: '',
  tooltip: '',
};

export function TextFormField(props: TextFormFieldProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.TextFormField,
    props: {...textFormFieldDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            label: this.label,
            value: this.value,
            tooltip: this.tooltip,
            onChange: events.onChangeWithValue(this),
            onBlur: events.onBlur(this),
            onFocus: events.onFocus(this),
            type: whisper.WhisperComponentType.TextInput,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type TextFormField = WrappedLDKComponent;
