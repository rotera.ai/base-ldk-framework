import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../../utils';
//import {whisper} from '@oliveai/ldk';
import {events} from '../../../eventBus';
import {WrappedWhisperComponentType} from '../../../types';

export type NumberFormFieldProps = {
  label: string;
  value?: number;
  max?: number;
  min?: number;
  step?: number;
  tooltip?: string;
};

export const numberFormFieldDefaultProps = {
  tooltip: '',
};

export function NumberFormField(props: NumberFormFieldProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.NumberFormField,
    props: {...numberFormFieldDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        const number: whisper.NumberInput = {
          label: this.label,
          tooltip: this.tooltip,
          onChange: events.onChangeWithValue(this),
          onBlur: events.onBlur(this),
          onFocus: events.onFocus(this),
          type: whisper.WhisperComponentType.Number,
        };

        if (this.value !== undefined) {
          number.value = this.value;
        }

        if (this.max !== undefined) {
          number.max = this.max;
        }

        if (this.min !== undefined) {
          number.min = this.min;
        }

        if (this.step !== undefined) {
          number.step = this.step;
        }

        return [number];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type NumberFormField = WrappedLDKComponent;
