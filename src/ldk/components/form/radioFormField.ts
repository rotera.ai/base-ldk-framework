import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../../utils';
import {events} from '../../../eventBus';
import {WrappedWhisperComponentType} from '../../../types';

/**
 * LDKRadioGroup
 *
 * A type wrapped around the LDK `RadioGroup` to give it a more meaningful name in our package.
 */
export type LDKRadioGroup = whisper.RadioGroup;

type RadioFormFieldProps = {
  options: string[];
  selected?: number;
};

export function RadioFormField(props: RadioFormFieldProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.RadioFormField,
    props: props,
    methods: {
      getComponents(): WhisperComponent[] {
        const radioGroup: LDKRadioGroup = {
          options: this.options,
          onSelect: events.onSelectWithValue(this),
          type: whisper.WhisperComponentType.RadioGroup,
        };

        if (this.selected !== null) {
          radioGroup.selected = this.selected;
        }

        return [radioGroup];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type RadioFormField = WrappedLDKComponent;
