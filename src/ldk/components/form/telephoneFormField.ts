import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../../utils';
import {events} from '../../../eventBus';
import {WrappedWhisperComponentType} from '../../../types';

type TelephoneFormFieldProps = {
  label: string;
  tooltip?: string;
  value?: string;
};

const telephoneFormFieldDefaultProps = {
  value: '',
  tooltip: '',
};

export function TelephoneFormField(props: TelephoneFormFieldProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.TelephoneFormField,
    props: {...telephoneFormFieldDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            label: this.label,
            value: this.value,
            tooltip: this.tooltip,
            onChange: events.onChangeWithValue(this),
            onBlur: events.onBlur(this),
            onFocus: events.onFocus(this),
            type: whisper.WhisperComponentType.Telephone,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type TelephoneFormField = WrappedLDKComponent;
