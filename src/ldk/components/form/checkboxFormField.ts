import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../../utils';
import {events} from '../../../eventBus';
import {WrappedWhisperComponentType} from '../../../types';

export type CheckboxFormFieldProps = {
  label: string;
  value?: boolean;
  tooltip?: string;
};

export const checkboxFormFieldDefaultProps = {
  value: false,
  tooltip: '',
};

export function CheckboxFormField(props: CheckboxFormFieldProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.CheckboxFormField,
    props: {...checkboxFormFieldDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            label: this.label,
            value: this.value,
            tooltip: this.tooltip,
            onChange: events.onChangeWithValue(this),
            type: whisper.WhisperComponentType.Checkbox,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type CheckboxFormField = WrappedLDKComponent;
