import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../../utils';
import {events} from '../../../eventBus';
import {WrappedWhisperComponentType} from '../../../types';

type PasswordFormFieldProps = {
  label: string;
  tooltip?: string;
  value?: string;
};

const passwordFormFieldDefaultProps = {
  value: '',
  tooltip: '',
};

export function PasswordFormField(props: PasswordFormFieldProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.PasswordFormField,
    props: {...passwordFormFieldDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            label: this.label,
            value: this.value,
            tooltip: this.tooltip,
            onChange: events.onChangeWithValue(this),
            onBlur: events.onBlur(this),
            onFocus: events.onFocus(this),
            type: whisper.WhisperComponentType.Password,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type PasswordFormField = WrappedLDKComponent;
