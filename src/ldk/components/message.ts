/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../utils';
import {WrappedWhisperComponentType} from '../../types';

type MessageProps = {
  header?: string;
  body: string;
  style?: whisper.Urgency;
  textAlign: whisper.TextAlign;
};

const messageDefaultProps = {
  header: '',
  style: whisper.Urgency.None,
  textAlign: whisper.TextAlign.Left,
};

export function Message(props: MessageProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.Message,
    props: {...messageDefaultProps, ...props},
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            header: this.header,
            body: this.body,
            textAlign: this.textAlign,
            style: this.style,
            type: whisper.WhisperComponentType.Message,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type Message = WrappedLDKComponent;
