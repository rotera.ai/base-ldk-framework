/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../utils';
import {WrappedWhisperComponentType} from '../../types';

type MarkdownProps = {
  body: string;
};

export function Markdown(props: MarkdownProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.Markdown,
    props: props,
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            body: this.body,
            type: whisper.WhisperComponentType.Markdown,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type Markdown = WrappedLDKComponent;
