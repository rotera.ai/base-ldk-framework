/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {Loop, renderComponents, WhisperComponent} from "../../utils";
import {Markdown} from "../../ldk/components/markdown";

/**
 * SimpleMessageWhisperProps
 *
 * Properties for the simple message whisper.
 */
export type SimpleMessageWhisperProps = {
  // The label for the whisper.
  label: string;

  // The content of the message.
  content: string;
};

/**
 * createSimpleMessageWhisper
 *
 * Create a simple message markdown whisper, which has only a label and a single message content.
 *
 * The message content can be markdown.
 *
 * @param props the properties for the window.
 * @returns (Whisper) markdown whisper built from props
 */
export async function createSimpleMessageWhisper(props: SimpleMessageWhisperProps): Promise<whisper.Whisper> {
  // TODO(Cameron/Keith): return to old version once Loop.createWhisper returns a whisper
  return await whisper.create({
    label: props.label,
    components: [{
      type: whisper.WhisperComponentType.Markdown,
      body: props.content,
    }],
  });
  /*
    Loop.createWhisper({
    props: props,
    data: {
      body: <Markdown>{},
    },
    methods: {
      getComponents(): WhisperComponent[] {
        this.body = Markdown({
          body: this.content,
        });
        return [this.body];
      },
    },

    lifeCycle: {
      async render() {
        try {
          await whisper.create({
            label: this.label,
            components: renderComponents(...this.getComponents()),
          });
        } catch (e) {
          console.error(e);
        }
      },
    },
  });
*/
}
