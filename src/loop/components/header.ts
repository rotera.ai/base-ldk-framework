/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../utils';
import {WrappedWhisperComponentType} from '../../types';

type HeaderProps = {
  header: string;
};

// TODO: Possibly create other components Header, h1, h2, h3

export function Header(props: HeaderProps): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.Header,
    props: props,
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            body: `# ${this.header}`,
            type: whisper.WhisperComponentType.Markdown,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type Header = WrappedLDKComponent;
