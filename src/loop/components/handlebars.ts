/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {
  Loop,
  renderComponents,
  WrappedLDKComponent,
  WhisperComponent,
  LDKComponent,
} from '../../utils';
import {WrappedWhisperComponentType} from '../../types';

type HandlebarsProps<D> = {
  // The data the handlebars template expects
  data: D;
  // The handlebars content as a function to call
  // @see https://github.com/pcardune/handlebars-loader
  tpl: Function;
};

/**
 * Handlebars
 *
 * In order to use this component the [handlebars-loader](https://github.com/pcardune/handlebars-loader) module must be used
 *
 */
export function Handlebars<D>(props: HandlebarsProps<D>): WrappedLDKComponent {
  return Loop.createComponent({
    type: WrappedWhisperComponentType.Handlebars,
    props: props,
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          {
            body: this.tpl(this.data),
            type: whisper.WhisperComponentType.Markdown,
          },
        ];
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}

export type Handlebars = WrappedLDKComponent;
