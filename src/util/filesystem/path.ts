import {system} from '@oliveai/ldk';

namespace path {

  /**
   * @const windowsOSName
   * @description the string returned by the system aptitude when the os is windows
   * @private
   */
  const windowsOSName = 'windows';

  /**
   * @const macOSName
   * @description the string returned by the system aptitude when the os is mac
   * @private
   */
  const macOSName = 'darwin';

  /**
   * @const win32SplitDeviceRe
   * @description Regex to split a windows path into three parts: [*, device, slash, tail] windows-only
   * @private
   */
  const win32SplitDeviceRe =
    /^([a-zA-Z]:|[\\\/]{2}[^\\\/]+[\\\/]+[^\\\/]+)?([\\\/])?([\s\S]*?)$/;

  /**
   * @const win32SplitTailRe
   * @description Regex to split the tail part of the above into [*, dir, basename, ext]
   * @private
   */
  const win32SplitTailRe =
    /^([\s\S]*?)((?:\.{1,2}|[^\\\/]+?|)(\.[^.\/\\]*|))(?:[\\\/]*)$/;

  /**
   * @function win32SplitPath
   * @description Function to split a filename into [root, dir, basename, ext]
   * @param path The file path
   * @return A string array consisting of [root, dir, basename, ext]
   * @private
   */
  function win32SplitPath(path: string): string[] {
    // Separate device+slash from tail
    const result = win32SplitDeviceRe.exec(path);
    let device: string;
    let tail: string;
    if (result) {
      device = result[1] + result[2];
      tail = result[3];
    } else {
      return ['', '', '', ''];
    }

    // Split the tail into dir, basename and extension
    const result2 = win32SplitTailRe.exec(tail);
    if (result2) {
      const dir = result2[1];
      const basename = result2[2];
      const ext = result2[3];

      return [device, dir, basename, ext];
    } else {
      return ['', '', '', ''];
    }
  }

  /**
   * @const posixsSlitPathRe
   * @description Split a filename into [root, dir, basename, ext], unix version, root will be a slash or nothing
   * @private
   */
  const posixSplitPathRe =
    /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;

  /**
   * @function posixSplitPath
   * @description Split a POSIX path into sections
   * @param filename The filename
   * @return a string array of the form [dir, basename, ext]
   * @private
   */
  function posixSplitPath(filename: string): string[] {
    const parts = posixSplitPathRe.exec(filename);
    if (parts) {
      return parts.slice(1);
    } else {
      return ['', '', ''];
    }
  }

  /**
   * @function basename
   * @description Get tge basename of a file path. Handles windows vs posix.
   * @param path The full path
   * @param ext The extension, if it should be removed
   * @returns The basename
   */
  export async function basename(path: string, ext?: string): Promise<string> {
    const os = await system.operatingSystem()
    let f: string
    if (os === windowsOSName) {
      f = win32SplitPath(path)[2];
    } else {
      f = posixSplitPath(path)[2];
    }

    // TODO: make this comparison case-insensitive on windows?
    if (ext && f.substring(-1 * ext.length) === ext) {
      f = f.substring(0, f.length - ext.length);
    }
    return f;
  }

  /**
   * @function extname
   * @description Get the extension of a file path. Handles windows vs posix.
   * @param path The full path
   * @param ext The extension, if it should be removed
   * @returns The extension
   */
  export async function extname(path: string): Promise<string> {
    const os = await system.operatingSystem();
    let f: string
    if (os === windowsOSName) {
      return win32SplitPath(path)[3];
    } else {
      return posixSplitPath(path)[2];
    }
  }
}

export default path;
