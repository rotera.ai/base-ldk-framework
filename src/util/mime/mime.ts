const db = require('mime-db')
import path from "../filesystem/path"

namespace mime {

  const types = Object.create(null);
  const extensions = Object.create(null)

  populateMaps(extensions, types);

  /**
   * Populate the extensions and types maps.
   * @private
   */

  function populateMaps (extensions: any, types:any) {
    // source preference (least -> most)
    var preference = ['nginx', 'apache', undefined, 'iana']

    Object.keys(db).forEach(function forEachMimeType (type) {
      const mime = db[type]
      const exts = mime.extensions

      if (!exts || !exts.length) {
        return
      }

      // mime -> extensions
      extensions[type] = exts

      // extension -> mime
      for (let i = 0; i < exts.length; i++) {
        const extension = exts[i]

        if (types[extension]) {
          const from = preference.indexOf(db[types[extension]].source)
          const to = preference.indexOf(mime.source)

          if (types[extension] !== 'application/octet-stream' &&
            (from > to || (from === to && types[extension].substr(0, 12) === 'application/'))) {
            // skip the remapping
            continue
          }
        }

        // set the extension -> mime
        types[extension] = type
      }
    })
  }

  export async function lookup(input: string): Promise<string | boolean> {
    const ext = await path.extname(input);
    const mimeType = types[ext];

    if (mimeType) {
      return mimeType;
    } else {
      return false;
    }
  }
}

export default mime;
