/**
 * NetworkError
 *
 * An error has happened during a network operation.
 */
export class NetworkError extends Error {
  constructor(message?: string) {
    super(message);
    this.name = 'NetworkError';
  }
}
