/**
 * authenticationHeaderProvider Module
 * A class that provides a wrapper for the simple ajax functions that fetches and provides an authentication
 * header using a provided AuthenticationProvider.
 *
 * The class supports typed HTTP requests and responded. GET requests will have the request object serialized
 * in the query string, while POST requests will have the body rendered as JSON before being placed
 * in the POST body. There is also support for multipart form data.
 * @module src/communication/middleware/authenticationHeaderProvider
 *
 * type TestRequest = {
 *   foo: string
 *   bar: number
 * }
 *
 *  type TestResponse = {
 *   bletch: string
 *   spam: number
 * }
 *
 * // This will be an appropriate authentication provider for your use case.
 * const authProvider = new AuthenticationProvider();
 * const httpProvider = new AuthenticationHeaderHttpProvider(authProvider);
 *
 * const response = httpProvider.httpRequest<TestRequest, TestResponse>(
 *   HttpMethods.POST,
 *   "https://api.someapi.com",
 *   {
 *     foo: "hi there,
 *     bar: 123
 *   });
 *
 * The response  above will be of type TestResponse.
 */

import {hasOwnProperty} from '../../utils';
import {
  fetchSimpleAjaxFormPostRequest,
  fetchSimpleAjaxGetRequest,
  fetchSimpleAjaxPostRequest,
} from '../api/simpleApiClients';
import {MultipartFormData} from '../api/multipartFormData';
import {AUTHORIZATION_HTTP_HEADER_NAME, AuthorizationType, HttpMethods} from "../api/http";
import { user } from "@oliveai/ldk";

/**
 * @type AuthenticationHeaderReturn - the object that must be returned from the authentication provider to create
 * the authentication header
 * @property authHeaderName {string} - the key for the header
 * @property authHeaderValue {string[]} - the value for the header
 */
export type AuthenticationHeaderReturn = {
  authHeaderName: string;
  authHeaderValue: string[];
}

/**
 * @interface AuthenticationProvider
 * @property canRetry {function} - returns true if a failed 401 call should be retried
 * @property getAuthenticationHeader {function} - returns the value for the authentication header
 */
export interface AuthenticationProvider {
  canRetry: () => boolean;
  getAuthenticationHeader: (retry?: boolean) => Promise<AuthenticationHeaderReturn>;
}


/**
 * @class FixedAuthenticationProvider
 * @description An authentication provider for fixed token values.
 * @property authReturn {AuthenticationHeaderReturn} - the fixed authentication return.
 */
export class FixedAuthenticationProvider implements AuthenticationProvider {

  authReturn: AuthenticationHeaderReturn;

  /**
   * @constructor
   * @param token {string} - the token that will be used for authorization.
   */
  constructor(token: string, headerName: string, authorizationType: AuthorizationType) {
    this.authReturn = {
      authHeaderName: headerName,
      authHeaderValue: [`${authorizationType} ${token}`]
    }
  }

  canRetry() {
    return false;
  }

  getAuthenticationHeader(retry? : boolean) {
    return Promise.resolve(this.authReturn)
  }

}

/**
 * @class FixedBearerAuthorizationAuthenticationProvider
 * @description An authorization provider that provides a fixed token as a bearer
 * @property authReturn {AuthenticationHeaderReturn} - the authentication return already created.
 */
export class FixedBearerAuthorizationAuthenticationProvider extends FixedAuthenticationProvider {

  /**
   * @constructor
   * @param token {string} - the token that will be used for authorization.
   */
  constructor(token: string) {
    super(token, AUTHORIZATION_HTTP_HEADER_NAME, AuthorizationType.BEARER)
  }
}


/**
 * @class LdkUserJWTAuthenticationProvider
 * @description An authorization provider uses the user aptitude to create a JWT
 * @property headerName {string} - the header name to be used.
 * @property authorizationType {AuthorizationType} - the authorization type to be used
 * @property jwt {string} - The Olive Helps user JWT.
 */
export class LdkUserJWTAuthenticationProvider implements AuthenticationProvider {
  headerName: string
  authorizationType: AuthorizationType
  jwt: string

  /**
   * @constructor
   * @param token {string} - the token that will be used for authorization.
   */
  constructor(headerName: string, authorizationType: AuthorizationType) {
    this.headerName = headerName;
    this.authorizationType = authorizationType;
  }

  canRetry() {
    return true;
  }

  async getAuthenticationHeader(retry = false) {
    if (this.jwt) {
      // We have a JWT. If we need to retry, then need to get a new one.
      if (retry) {
        await this.getJwt();
      }
    } else {
      // No JWT yet.
      await this.getJwt();
    }

    const authReturn = {
      authHeaderName: this.headerName,
      authHeaderValue: [`${this.authorizationType} ${this.jwt}`]
    }

    return authReturn;
  }

  /**
   * @function getJwt
   * @description Get the user JWT and place it in the class property.
   */
  async getJwt() {
    this.jwt = await user.jwt({
      includeEmail: true
    });
  }
}

/**
 * @class AuthenticationHeaderProvider
 * Provides a function, httpRequest, that will add an authentication header, using the AuthenticationProvider
 * the class was constructed with.
 */
export class AuthenticationHeaderHttpProvider {
  authenticationProvider: AuthenticationProvider;

  /**
   * @constructor
   * @param tokenProvider {AuthenticationProvider} - will be used to get the value for the authentication header
   */
  constructor(tokenProvider: AuthenticationProvider) {
    this.authenticationProvider = tokenProvider;
  }

  /**
   * Makes an authenticated http call, HTTP method decided by http method parameter.
   * If the call comes back with a 401, the call will be reattempted with a new token
   * if the token provider is retriable.
   * @param httpMethod {HttpMethod} - one of the supported http methods
   * @param url {string} - the url to call
   * @param request {Request} - The request to be sent
   * @param headers {string} - any additional headers to add
   * @returns {Response} - the response from the http function
   */
  async httpRequest<Request, Response>(
    httpMethod: HttpMethods,
    url: string,
    request: Request,
    headers?: Record<string, string[]>
  ): Promise<Response> {
    headers = headers || {}
    let createdHeaders = await this.createHeaders(headers, false);
    const response = await this.getHttpFunction(httpMethod)(
      url,
      request,
      createdHeaders
    );
    if (
      this.authenticationProvider.canRetry() &&
      response &&
      typeof response === 'object' &&
      hasOwnProperty(response, 'statusCode') &&
      response.statusCode === 401
    ) {
      createdHeaders = await this.createHeaders(headers, true);
      return await this.getHttpFunction(httpMethod)(
        url,
        request,
        createdHeaders
      );
    }
    return response as Response;
  }

  /**
   * Makes an authenticated http multipart form post,
   * If the call comes back with a 401, the call will be reattempted with a new token
   * if the token provider is retriable.
   * @param httpMethod {HttpMethod} - one of the supported http methods
   * @param url {string} - the url to call
   * @param request {MultipartFormData} - The request to be sent
   * @param headers {string} - any additional headers to add
   * @returns {Response} - the response from the http function
   */
  async formPostHttpRequest<Response>(
    url: string,
    request: MultipartFormData,
    headers?: Record<string, string[]>
  ): Promise<Response> {
    headers = headers || {}
    let createdHeaders = await this.createHeaders(headers, false);
    const response = await fetchSimpleAjaxFormPostRequest(
      url,
      request,
      createdHeaders
    );
    if (
      this.authenticationProvider.canRetry() &&
      response &&
      typeof response === 'object' &&
      hasOwnProperty(response, 'statusCode') &&
      response.statusCode === 401
    ) {
      createdHeaders = await this.createHeaders(headers, true);
      return await fetchSimpleAjaxFormPostRequest(
        url,
        request,
        createdHeaders
      );
    }
    return response as Response;
  }

  /**
   * Given headers, return an object with those headers and a token header
   * the authHeader needs to be of type record<string, string[]>
   * @param jwt {string} - the jwt to use as a bearer token
   * @param retry {boolean} - true if this is a retry for authentication
   * @returns {object} - an object with a token and api key header
   */
  async createHeaders(
    headers: Record<string, string[]>,
    retry: boolean
  ): Promise<Record<string, string[]>> {
    const {authHeaderName, authHeaderValue} =
      await this.authenticationProvider.getAuthenticationHeader(retry);
    return {
      [authHeaderName]: authHeaderValue,
      ...headers,
    };
  }

  /**
   * Given an HttpMethod, return the function that will do the call
   * corresponing to the given http verb
   * @param httpMethod {HttpMethod} - the http verb to be used
   * @returns {function} - function that will make an http call
   */
  getHttpFunction(httpMethod: HttpMethods) {
    switch (httpMethod) {
      case HttpMethods.GET:
        return fetchSimpleAjaxGetRequest;
      case HttpMethods.POST:
        return fetchSimpleAjaxPostRequest;
    }
  }
}
