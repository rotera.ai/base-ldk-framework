/**
 * simpleApiClients Module
 *
 * The methods in this file support typed HTTP requests and responded. GET requests will have the request object serialized
 * in the query string, while POST requests will have the body rendered as JSON before being placed
 * in the POST body. There is also support for multipart form data.
 * @module src/communication/api/simpleApiClients
 *
 * type TestRequest = {
 *   foo: string
 *   bar: number
 * }
 *
 *  type TestResponse = {
 *   bletch: string
 *   spam: number
 * }
 *
 * const response = fetchSimpleAjaxPostRequest<TestRequest, TestResponse>(
 *   "https://api.someapi.com",
 *   {
 *     foo: "hi there,
 *     bar: 123
 *   });
 *
 * The response  above will be of type TestResponse.
 */

import {network} from '@oliveai/ldk';
import {NetworkError} from "../errors/errors";
import {MultipartFormData} from './multipartFormData';

/**
 * decodeResponseData
 *
 * Decode the response data from a network query.
 */
export async function decodeResponseData(
  response: network.HTTPResponse
): Promise<string> {
  try {
    return await network.decode(response.body);
  } catch (error) {
    throw new NetworkError('helpers/api at decodeResponseData, ' + error);
  }
}

/**
 * buildQueryString
 *
 * Build a query string from some parameters. All values will be properly URL-encoded.
 *
 * @param params An object containing the params with keys as parameter names
 *
 * @return A properly encoded URL query string.
 */
export function buildQueryString(params?: Object): string {
  if (!params) {
    return '';
  }

  const queryString = Object.keys(params)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent((params as any)[key])}`)
    .join('&');
  return queryString ? `?${queryString}` : '';
}

/**
 * fetchSimpleAjaxGetRequest
 *
 * Do a GET-based AJAX call. The request object, if any, is serialized as query string parameters.
 *
 * @param url The URL endpoint for the API call
 * @param request The request for the API call, if any
 * @param headers Any additional headers to be attached to the call
 * @return {Response} The API response object
 */
export async function fetchSimpleAjaxGetRequest<Request, Response>(
  url: string,
  request?: Request,
  headers?: Record<string, string[]>
): Promise<Response> {
  try {
    if (request) {
      url = url + buildQueryString(request);
    }

    const rawResponse = await network.httpRequest({
      method: 'GET',
      headers,
      url: url,
    });

    const responseData = await decodeResponseData(rawResponse);
    const response = JSON.parse(responseData);

    return (response as unknown) as Response;
  } catch (error) {
    throw new NetworkError('Could not process AJAX GET request, ' + error);
  }
}

/**
 * fetchSimpleAjaxPostRequest
 *
 * Do a POST-based AJAX call. The request object is serialized as a JSON object for the POST body.
 *
 * @param url The URL endpoint for the API call
 * @param request The request for the API call
 * @param headers Any additional headers to be attached to the http call
 * @return {Response} The API response object
 */
export async function fetchSimpleAjaxPostRequest<Request, Response>(
  url: string,
  request: Request,
  headers?: Record<string, string[]>
): Promise<Response> {
  try {
    headers = headers || {};

    const rawResponse = await network.httpRequest({
      method: 'POST',
      headers: {
        "content-type": ["application/json"],
        ...headers
      },
      url: url,
      body: await network.encode(JSON.stringify(request)),
    });

    const responseData = await decodeResponseData(rawResponse);
    const response = JSON.parse(responseData);

    return (response as unknown) as Response;
  } catch (error) {
    throw new NetworkError('Could not process AJAX POST request, ' + error);
  }
}

/**
 * fetchSimpleAjaxFormPostRequest
 *
 * Do a POST-based AJAX call. The request object is serialized as a JSON object for the POST body.
 *
 * @param url The URL endpoint for the API call
 * @param request The form request for the API call
 * @param headers Any additional headers to be attached to the http call
 * @return {Response} The API response object
 */
export async function fetchSimpleAjaxFormPostRequest<Response>(
  url: string,
  request: MultipartFormData,
  headers?: Record<string, string[]>
): Promise<Response> {
  headers = headers || {};

  try {
    const finalHeaders:  Record<string, string[]> = {
      ...headers,
      ...request.getHeaders()
    }

    const rawResponse = await network.httpRequest({
      method: 'POST',
      headers: finalHeaders,
      url: url,
      body: request.getBuffer(),
    });

    const responseData = await decodeResponseData(rawResponse);
    const response = JSON.parse(responseData);

    return (response as unknown) as Response;
  } catch (error) {
    throw new NetworkError('Could not process AJAX POST request, ' + error);
  }
}
