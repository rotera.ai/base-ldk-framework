/**
 * A form data library for the creation of multipart/form posts.
 *
 * This library was created because Olive Helps will crash if the fs library is loaded.
 * All libraries not supported by Olive Helps were removed.
 *
 * const formData = new FormData();
 * formData.append("foo", "bar");
 * formData.append("bletch", "spam");
 * formData.append("yada", Buffer.from([1,2,3,4,5,6,9,8,8]), {
 *      filename: "foo.pdf"
 * });
 *
 * This code take the https://www.npmjs.com/package/form-data, removes everything to do
 * with non-Olive Helps supported part types, and rewrites it in TypeScript.
 */
import {CONTENT_TYPE_HTTP_HEADER_NAME} from "./http";
import path from '../../util/filesystem/path';
const Buffer = require('buffer/').Buffer;

/**
 * @const LINE_BREAK
 * @description The line break character combination for the multipart/form specification.
 */
const LINE_BREAK = '\r\n';

/**
 * @const DEFAULT_CONTENT_TYPE
 * @description The default content-type of a part if one is not supplied and cannot be determined automatically.
 */
const DEFAULT_CONTENT_TYPE = 'application/octet-stream';

/**
 * @type Headers
 * @description A collection of headers.
 */
export type Headers = Record<string, string[]>;

/**
 * @type AppendOptions
 * @description Options for appending a value into the form.
 * @property header Any headers to be added to the form part
 * @property knownLength The legth of the part, if already known
 * @property filename The filename the part goes into the form with
 * @property filepath The path of the form part.
 * @property contentType The type of the content, though can be calculated from the file name or path.
 */
export type AppendOptions = {
  header?: string | Headers;
  knownLength?: number;
  filename?: string;
  filepath?: string;
  contentType?: string;
}

/**
 * @type MultipartFormPart
 * @desription a part of the form.
 * @property headers The headers for the part
 * @property value The value of the part
 */
type MultipartFormPart = {
  headers: string
  value: any
}

/**
 * @class MultipartFormData
 * @description A class for creating multipart/form HTTP post bodies.
 * @property _boundary The boundary string fot the body portions
 * @property _valueLength The length in bytes of the form body
 * @property _overheadLength The number of bytes of overhead
 * @property _formParts All the contents in the form
 */
export class MultipartFormData {
  _boundary: string
  _valueLength: number
  _overheadLength: number
  _formParts: MultipartFormPart[]

  /**
   * @constructor The constructor for form data.
   */
  constructor() {
    this._formParts = []
  }

  /**
   * @function append
   * @description Append in a new field into the form. For now, content type will not be determined by filename extensions.
   * @param field The name of the field.
   * @param value The value of a field. Includes Buffers.
   * @param options The options for the append, such as a filename, content type, etc.
   */
  async append(field: string, value: any, options?: AppendOptions | string): Promise<void> {
    options = options || {};

    // allow filename as single option
    if (typeof options == 'string') {
      options = {filename: options};
    }

    // all that streamy business can't handle numbers
    if (typeof value == 'number') {
      value = '' + value;
    }

    const header = await this._multiPartHeader(field, value, options);

    this._formParts.push({
      headers: header,
      value: value
    });

    this._trackLength(header, value, options);
  }

  /**
   * @function getHeaders
   * @description Get all headers needed by the form, primarily the content type header
   * @param userHeaders Any additional headers to add
   * @return the content header along with the user headers
   */
  getHeaders(userHeaders?: Headers): Headers {
    const formHeaders: Headers = {
      [CONTENT_TYPE_HTTP_HEADER_NAME]: ['multipart/form-data; boundary=' + this.getBoundary()]
    };

    for (const header in userHeaders) {
      if (userHeaders.hasOwnProperty(header)) {
        formHeaders[header.toLowerCase()] = userHeaders[header];
      }
    }

    return formHeaders;
  }

  /**
   * @function getBuffer()
   * @description Get a buffer of the form content.
   * @return {Buffer} a buffer of the form content.
   */
  getBuffer(): Buffer {
    let dataBuffer = Buffer.alloc(0);
    let boundary = this.getBoundary();

    // Create the form content. Add Line breaks to the end of data.
    for (let i = 0, len = this._formParts.length; i < len; i++) {
      const formPart = this._formParts[i];
      dataBuffer = Buffer.concat([dataBuffer, Buffer.from(formPart.headers, 'utf8')]);

      // Add content to the buffer.
      if (Buffer.isBuffer(formPart.value)) {
        dataBuffer = Buffer.concat([dataBuffer, formPart.value]);
      } else {
        dataBuffer = Buffer.concat([dataBuffer, Buffer.from(formPart.value, 'utf8')]);
      }
      dataBuffer = Buffer.concat([dataBuffer, Buffer.from(LINE_BREAK, 'utf8')]);
    }

    // Add the footer and return the Buffer object.
    const finalBuffer = Buffer.concat([dataBuffer, Buffer.from(this._lastBoundary(), 'utf8')]);

    return finalBuffer;
  }

  /**
   * @function setBoundary
   * @param boundary Set the string that will become the part boundary in the form
   */
  setBoundary(boundary: string): void {
    this._boundary = boundary;
  }

  /**
   * @function getBoundary
   * @description get the string that will become the part boundary in the form
   * @return {string} string
   */
  getBoundary(): string {
    if (!this._boundary) {
      this._generateBoundary();
    }

    return this._boundary;
  }

  /**
   * @function getLength
   * @description Get the length of the content added in so far. Will include the final boundary.
   * @return {number} the number of bytes ibn the form
   */
  getLength(): number {
    let totalLength = this._overheadLength + this._valueLength;

    if (this._formParts.length) {
      totalLength += this._lastBoundary().length;
    }

    return totalLength
  }

  /**
   * @function _multiPartHeader
   * @description Create the appropriate multipart part header for a field in the form.
   * @param field The field name
   * @param value The value of the field
   * @param options The options for adding the field
   * @return A string of the header for the form part
   */
  async _multiPartHeader(field: string, value: any, options: AppendOptions): Promise<string> {
    // custom header specified (as string)?
    // it becomes responsible for boundary
    // (e.g. to handle extra CRLFs on .NET servers)
    if (typeof options.header == 'string') {
      return options.header;
    }

    const contentDisposition = await this._getContentDisposition(value, options);
    const contentType = this._getContentType(value, options);
    let contents = '';
    const headers: Headers = {
      // add custom disposition as third element or keep it two elements if not
      'Content-Disposition': ['form-data', 'name="' + field + '"'].concat(contentDisposition || []),
      // if no content type. allow it to be empty array
      'Content-Type': contentType ? [contentType] : []
    };

    // allow custom headers.
    if (typeof options.header == 'object') {
      populate(headers, options.header);
    }

    let header;
    for (const prop in headers) {
      if (!headers.hasOwnProperty(prop)) continue;
      header = headers[prop];

      // skip nullish headers.
      if (header == null) {
        continue;
      }

      // convert all headers to arrays.
      if (!Array.isArray(header)) {
        header = [header];
      }

      // add non-empty headers.
      if (header.length) {
        contents += prop + ': ' + header.join('; ') + LINE_BREAK;
      }
    }

    return '--' + this.getBoundary() + LINE_BREAK + contents + LINE_BREAK;
  }

  /**
   * @function _getContentDisposition
   * @description Get the content disposition header for this form part, if any
   * @param value The value of the field
   * @param options The options for adding the field
   * @return the header, if any
   */
  async _getContentDisposition(value: any, options: AppendOptions): Promise<string | undefined> {
    let filename, contentDisposition;

    if (options.filename || value.name || value.path) {
      // custom filename take precedence
      // formidable and the browser add a name property
      // fs- and request- streams have path property
      filename = await path.basename(options.filename || value.name || value.path);
    }

    if (filename) {
      contentDisposition = 'filename="' + filename + '"';
    }

    return contentDisposition;
  }

  /**
   * @function _getContentType
   * @description Get the content type header for this form part, if any
   * @param value The value of the field
   * @param options The options for adding the field
   * @return the header, if any
   */
  _getContentType(value: any, options: AppendOptions): string | undefined {

    // use custom content-type above all
    let contentType: string | undefined = options.contentType;

    /*
    TODO(keith): Make mimetype lookup work.
    // or try `name` from formidable, browser
    if (!contentType && value.name) {
      contentType = mime.lookup(value.name);
    }

    // or try `path` from fs-, request- streams
    if (!contentType && value.path) {
      contentType = mime.lookup(value.path);
    }

    // or guess it from the filepath or filename
    if (!contentType && (options.filepath || options.filename)) {
      contentType = mime.lookup(options.filepath || options.filename);
    }
    */

    // fallback to the default content type if `value` is not simple value
    if (!contentType && typeof value == 'object') {
      contentType = DEFAULT_CONTENT_TYPE;
    }

    return contentType;
  };

  /**
   * @function _lastBoundary
   * @description get the final boundary for the form body
   * @return the final boundary marker
   */
  _lastBoundary() {
    return '--' + this.getBoundary() + '--' + LINE_BREAK;
  }

  /**
   * @function _generateBoundary
   * @description generate a biundary value
   * @return the generated boundary
   */
  _generateBoundary() {
    // This generates a 50 character boundary similar to those used by Firefox.
    // They are optimized for boyer-moore parsing.
    let boundary = '--------------------------';
    for (let i = 0; i < 24; i++) {
      boundary += Math.floor(Math.random() * 10).toString(16);
    }

    this._boundary = boundary;
  }

  /**
   * @function _trackLength
   * @description Calculate the length a part will add to the form
   * @param value The value of the field
   * @param options The options for adding the field
   */
  _trackLength(header: string, value: any, options: AppendOptions): void {
    let valueLength = 0;

    // used w/ getLengthSync(), when length is known.
    // e.g. for streaming directly from a remote server,
    // w/ a known file a size, and not wanting to wait for
    // incoming file to finish to get its size.
    if (options.knownLength != null) {
      valueLength += +options.knownLength;
    } else if (Buffer.isBuffer(value)) {
      valueLength = value.length;
    } else if (typeof value === 'string') {
      valueLength = Buffer.byteLength(value);
    }

    this._valueLength += valueLength;

    // @check why add CRLF? does this account for custom/multiple CRLFs?
    this._overheadLength +=
      Buffer.byteLength(header) +
      LINE_BREAK.length;
  }
}

/**
 * @function populate
 * @descriptin Make sure that the destination has all of the keys and values found in src. If  the destination
 * contains a key already, it will not receive the value from the source. The destination is modied in place
 * @param dst The destination object
 * @param src The source object
 * @return The destination object.
 */
function populate(dst: any, src: any) {
  Object.keys(src).forEach((prop) => {
    dst[prop] = dst[prop] || src[prop];
  });

  return dst;
}
