
/**
 * @enum HttpMethods {string}
 * @property GET - denotes a get call
 * @property POST - denotes a post call
 */
export enum HttpMethods {
  GET = 'GET',
  POST = 'POST',
}

/**
 * @enum AuthorizationType {string} The types of authorization headers
 * @property BASIC - The Basic authorization type.
 * @property BEARER - The Bearer authorization type.
 */
export enum AuthorizationType {
  BASIC = 'Basic',
  BEARER = 'Bearer'
}

/**
 * The HTTP header name for the authorization header.
 */
export const AUTHORIZATION_HTTP_HEADER_NAME = 'authorization'

/**
 * The HTTP header name for the content type header.
 */
export const CONTENT_TYPE_HTTP_HEADER_NAME = 'content-type'
