/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

export enum WrappedWhisperComponentType {
  CheckboxFormField = 'checkboxFormField',
  EmailFormField = 'emailFormField',
  NumberFormField = 'numberFormField',
  PasswordFormField = 'passwordFormField',
  RadioFormField = 'radioFormField',
  SelectFormField = 'selectFormField',
  TelephoneFormField = 'telephoneFormField',
  TextFormField = 'textFormField',
  Button = 'button',
  Link = 'link',
  ListPair = 'listPair',
  Markdown = 'markdown',
  Message = 'message',
  Handlebars = 'handlebars',
  Header = 'header',
  CollapseBox = 'collapseBox',
}