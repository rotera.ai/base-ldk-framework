/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import * as component from './components';
import * as eventBus from './eventBus';
import * as type from './types';
import * as util from './utils';

const BaseLDKFramework = {
  eventBus,
  type,
  util,
  component
};

export default BaseLDKFramework;
