# Rotera Base LDK Framework

The Rotera Base Loop Development Kit (LDK) Library provides a set of abstractions
over portions of the Olive Helps Loop Development Kit
(Olive LDK, [https://docs.oliveai.dev/ldk](https://docs.oliveai.dev/ldk)).

These abstractions include wrappers around Whisper components and functions/classes
that simplify and standardize usage of the various aptitudes. Examples of aptitude
functions include simple typed AJAX calls.

This library is used every day in Rotera's API development. For more details, see
[https://www.rotera.ai](https://www.rotera.ai).

# TOC
- [Wrapped Components](#wrapped-components)
  - [Example of wrapped LDK component](#example-of-wrapped-ldk-component)
  - [Example of a custom component](#example-of-a-custom-component)
  - [Example of listening for events from child components](#example-of-listening-for-events-from-child-components)
  - [Example without event listener](#example-without-event-listener)
- [Tooling](#tooling)
- [TODOs](#todo)

# Wrapped Components
The LDK components have been wrapped to reduce duplicated code and to establish a methodology around creating components. In return, it should be easier for engineers to jump between projects and to start developing loops faster as it will require less initial setup. It also helps with future maintenance because teams can manage the LDK components within one location vs many.

## Adding new wrapped components
All wrapped LDK components should be created in `src/ldk/components`.

All wrapped custom components should be created in `src/loop/components`.

If you create a new component you'll need to add it to `src/components` and create additional tests.

## Creating whispers and components within your loop project
All components should be created with `Loop.createComponent()`.

All whispers should be created with `Loop.createWhisper()`.

## Component/whisper property exposure (private/public)
Only certain methods should be exposed and callable from a parent component or whisper. The idea is to have control over the "thing" that is being manipulated and to avoid deeply nested components controlling parent components. Public methods are determined by the return type's properties in `createComponent()`. Refer to `WrappedComponent` for additional details.

If child components need to be manipulated the EventBus can be utilized. If there's a need to handle state in a better way then implementing some type of global store may be ideal.

#### Exposed methods/properties
- LoopEvents.on
- LoopEvents.off
- LoopEvents.emit
- LifeCycle.render
- LifeCycle.destroy
- type

#### Recommended methods for whisper
Some optional methods have been added to both component and whisper types. Use these methods to provide a consistent structure when developing and add additional methods as needed.

- Methods.getData
- Methods.getComponents
- Methods.addEventListeners
- Methods.removeEventListeners

# Example of wrapped LDK component
Any new LDK components that need to be created should follow the pattern below.

```ts
// Configurable properties
type Props = {};
// Default property values. Any optional property in `Props` should
// have a default value set in `defaultProps`.
const defaultProps = {};

// Use a function declaration.
// Use PascalCase for component names.
export function TheLDKComponentToBeWrapped(props: Props): WrappedComponent {
  // The component's `props`, `data`, `methods`, `lifeCycle` should all be accessed with `this`.
  // The component must be returend so public methods can be accessed.
  return Loop.createComponent({
    // The component's configurable properties referred to as `props`.
    // `defaultProps` must always be set first.
    // Required and optional `props` will override `defaultProps`.
    props: {...defaultProps, ...props},
    // The component's collection of data and state referred to as `data`.
    // This clearly defines what data is being used in the component.
    // The value can be set in `data` or later after an API request.
    // `this` scope is not accessible in `data`.
    //
    // Ensure that there are no duplicate/overlapping variables between `props` and `data`
    data: {},
    // Where all methods will live.
    // The component should always contain `getComponents()` for consistency.
    // It clearly shows what individual components make up this component.
    // It's ok to have other methods that build more complex components, but
    // `getComponents()` should be called from `render()`.
    //
    // For consistencey `getComponents()` will always return an array. Some 
    // wrapped components may be more complex than others, containing more 
    // than one component.
    methods: {
      getComponents(): WhisperComponent[] {
        return [];
      },
    },
    // The various life cycles of the component.
    lifeCycle: {
      // `render()` is always required and should always return `LDKComponent[]`.
      render(): LDKComponent[] {
        // For consistency, `renderComponents` should always be used to properly render the components to a LDK component.
        return renderComponents(...this.getComponents());
      },
    },
  });
}
```
# Example of a custom component

```ts
type Props = {
  header: string;
  name: string;
  dob: number;
  mrn: string;
  phone: string;
}

export function PatientInfo(props: Props): WrappedComponent {
  return Loop.createComponent({
    props: props,
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          // Header
          HeaderComponent({header: this.header}),
          // Patient name
          MarkdownComponent({
            body: `${lang.ui.general.name}: ${this.name}`,
          }),
          // Date of birth
          MarkdownComponent({
            body: `${lang.ui.general.dob}: ${new Date(
              this.dob
            ).toLocaleDateString()}`,
          }),
          // MRN
          MarkdownComponent({
            body: `${lang.ui.general.mrn}: ${this.mrn}`,
          }),
          // Phone
          MarkdownComponent({
            body: `${lang.ui.general.phoneNumber}: ${this.phone}`,
          })
        ]
      },
    },
    lifeCycle: {
      render(): LDKComponent[] {
        return renderComponents(...this.getComponents());
      },
    },
  });
}
```

# Example of listening for events from child components
In order to listen for events from the wrapped components we'll need to implement an `EventBus` mechanism. Our components will `emit` an event, and the parent component or whisper will listen for it via `on`.

```ts
export function SomeWhisper() {
  Loop.createWhisper({
    components: {
      // Since we're listening for events, the components must be registered so they're 
      // accessible in other methods. For consistency, if one component is listening
      // for an event then create all components in the same manner. e.g., `header`
      // doesn't emit events, but it's still registered and created like the other
      // components.
      header: <Header>{},
      preferences: <Preference>{},
      preferencesBtn: <Button>{}
    },
    getComponents(): WhisperComponent[] {
      this.header = Header({header: 'Whisper Header'});

      this.preferences = Preferences({
        header: 'User Preferences',
        labelLow: 'Low',
        labelHigh: 'High',
      });

      this.preferencesBtn = Button({label: 'Update'});

      return [
        this.header,
        this.preferences,
        this.preferencesBtn
      ];
    },
    // Listen for all events
    addEventListeners() {
      // Preferences listening for multiple click events
      this.preferences.on('click-low', this.handlePreferencesClick.bind(this));
      this.preferences.on('click-high', this.handlePreferencesClick.bind(this));
      // Button listening for single event
      this.preferencesBtn.on('click', this.handleButtonClick.bind(this));
    },
    // To prevent memory leaks we'll move all event listeners
    removeEventListeners() {
      this.preferences.off('click-low', this.handlePreferencesClick);
      this.preferences.off('click-high', this.handlePreferencesClick);
      this.preferencesBtn.off('click', this.handleButtonClick);
    },
    lifeCycle: {
      async render() {
        try {
          await whisper.create({
            label: 'Whisper Label',
            components: renderComponents(...this.getComponents()),
            onClose: () => this.destroy(),
          });
        } catch (e) {
            console.error(e);
        }
      },
      // Called from whisper
      destroy() {
        this.removeEventListeners();
      },
    }
  })
}
 ```

# Example without event listener
The most basic implementation of creating whispers.

```ts
export function SomeWhisper() {
  Loop.createWhisper({
    // Since none of the components emit events we can simplify 
    // their creation because they don't need to be accessible in 
    // any other methods. There's no need to register a component
    // within the `components` object unless you just want to stay 
    // consistent throughout the entirety of the project whether a 
    // component emits events or is needed to be accessible in other methods.
    getComponents(): WhisperComponent[] {
      return [
          Header({header: 'Whisper Header'}),
          Markdown({body: 'Lorem ipsum...'})
      ];
    },
    lifeCycle: {
      async render() {
        try {
            await whisper.create({
              label: 'Whisper Label',
              components: renderComponents(...this.getComponents()),
            });
        } catch (e) {
            console.error(e);
        }
      },
    }
  })
}
 ```

# Tooling
- As of 07/2021 there's not a reliable solution to resolve path aliases when transpiling code for production. Since this is a package module we need to make sure the aliases resolve, if not none of the modules will be found during import. For now, we'll just rely on relative URLs vs path alias.
- We'll use `tsc` to transpile our package. As of now we don't need the features of Webpack.


# TODO
- Add `renderComponents()` as a default method, so we don't need to include it in every single component/whisper. It should be callable with `this.renderComponents()`.
- I would like to find **one** way of creating components, whether they have event listeners or not. (e.g. "Example of listening for events from child components" vs "Example without event listener"). Even though both components make sense in their own implementation, it still bugs me they're different. The team may just want to decide on one particular direction. Personally, I'm leaning towards the always registering components in `components` object. For one, it gives a high-level view of what's going on and provides meaningful names to each component.
  

