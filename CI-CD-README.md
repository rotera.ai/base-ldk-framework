# CI/CD

The Rotera opensource projects uses Gitlab to build and publish versions of the projects.

The projects are developed on Rotera's gitlab and push to https://gitlab.com/rotera.ai when released to the
public.

## Gitlab Runner Variables

The CD/CD pipeline needs a variety of Gitlab Runner Variables set for the builds to work properly.

### ROTERA_GITLAB_HOST

This variable specifies the Gitlab host where the project is being hosted.

### ROTERA_GITLAB_REGISTRY_ID

This variable specifies the Gitlab ID of the project which contains the NPM registry.

### ROTERA_REGISTRY_TOKEN

This variable specifies the access token used to write to the NPM registry.

## Notes

The `.releaserc.json` is overwritten by the CI/CD pipeline.

