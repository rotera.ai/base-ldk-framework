/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {
  Loop,
  WhisperComponent,
  renderComponents,
} from '@rotera.ai/base-ldk-framework/dist/utils';
import {
  Header,
  Markdown,
  Divider,
  Link,
} from '@rotera.ai/base-ldk-framework/dist/components';
import {
  WhisperComponentType,
} from '@oliveai/ldk/dist/whisper';

export function WhisperBox() {
  Loop.createWhisper({
    methods: {
      getPrePostComponents(): WhisperComponent[] {
        return [
          Header({header: 'Natus error sit voluptatem.'}),
          Markdown({
            body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
          }),
          Divider()
        ]
      },
      getCollapsibleComponents() {
        return [
          Header({header: 'Inventore veritatis'}),
          Markdown({
            body:
              'Sed ut perspiciatis unde omnis iste natus error sit voluptatem.',
          }),
          Link({text: 'External Link', href: 'https://www.rotera.ai/'}),
          Divider(),
          Header({header: 'Nemo enim ipsam'}),
          Markdown({
            body:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
          }),
        ];
      },
      getComponents(callableComponents: Function): WhisperComponent[] {
        return callableComponents();
      },
    },
    lifeCycle: {
      async render() {
        try {
          await whisper.create({
            label: 'Collapse box example',
            components: [
              // Note that at some point we'll implement the Box and CollapseBox components into the base-ldk-framework.
              // By doing so, it'll be a bit cleaner than what's below because we'll only need to supply the components
              // without usin gthe `renderComponents`, the spread operator, and the mix of functions and objects.
              ...renderComponents(...this.getComponents(this.getPrePostComponents)),
              {
                type: WhisperComponentType.CollapseBox,
                open: false,
                children: renderComponents(...this.getComponents(this.getCollapsibleComponents)),
              },
              ...renderComponents(...this.getComponents(this.getPrePostComponents)),
            ],
          });
        } catch (e) {
          console.log(e);
        }
      },
    },
  });
}
