/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {
  Loop,
  WhisperComponent,
  renderComponents,
} from '@rotera.ai/base-ldk-framework/dist/utils';
import {
  Header,
  Markdown,
  Divider,
  Link,
} from '@rotera.ai/base-ldk-framework/dist/components';

export function WhisperSimple() {
  Loop.createWhisper({
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          Header({header: 'Inventore veritatis'}),
          Markdown({
            body: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem.',
          }),
          Link({text: 'External Link', href: 'https://www.rotera.ai/'}),
          Divider(),
          Header({header: 'Nemo enim ipsam'}),
          Markdown({
            body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
          }),
        ];
      },
    },
    lifeCycle: {
      async render() {
        try {
          await whisper.create({
            label: 'Whisper simple',
            components: renderComponents(...this.getComponents()),
          });
        } catch (e) {
          console.log(e);
        }
      },
    },
  });
}
