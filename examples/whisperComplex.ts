/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {Loop, WhisperComponent, renderComponents} from '@rotera.ai/base-ldk-framework/dist/utils';
import {Header, Markdown} from '@rotera.ai/base-ldk-framework/dist/components';

type Props = {
  mrn: string;
};

export function WhisperComplex(props: Props) {
  Loop.createWhisper({
    props: props,
    data: {
      patient: {} as Record<string, any>,
    },
    methods: {
      async getData() {
        try {
          // Where you can make API and other requests
          this.patient.glucoseReadings = [0, 1, 2];
          this.patient.bloodPressureReadings = [4, 5, 6];
        } catch (e) {
          console.error(e);
        }
      },
      getGlucoseComponents(): WhisperComponent[] {
        return [
          Header({header: 'Glucose'}),
          Markdown({body: this.patient.glucoseReadings.toString()}),
        ];
      },
      getBloodPressureComponents(): WhisperComponent[] {
        return [
          Header({header: 'Blood Pressure'}),
          Markdown({body: this.patient.bloodPressureReadings.toString()}),
        ];
      },
      getComponents(someArg: string): WhisperComponent[] {
        return [
          Header({header: someArg}),
          Markdown({body: `The mrn is ${this.mrn}`}),
          ...this.getGlucoseComponents(),
          ...this.getBloodPressureComponents(),
        ];
      },
    },
    lifeCycle: {
      async render() {
        try {
          await this.getData();

          const param = 'Some var declared from additional logic';

          await whisper.create({
            label: 'Whisper complex',
            components: renderComponents(...this.getComponents(param)),
          });
        } catch (e) {
          console.log(e);
        }
      },
    },
  });
}
