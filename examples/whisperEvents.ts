/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {Whisper} from '@oliveai/ldk/dist/whisper/types';
import {
  Loop,
  WhisperComponent,
  renderComponents,
} from '@rotera.ai/base-ldk-framework/dist/utils';
import {
  Header,
  Markdown,
  Button,
  SelectFormField,
  RadioFormField,
  CheckboxFormField,
} from '@rotera.ai/base-ldk-framework/dist/components';
import {EventType} from '@rotera.ai/base-ldk-framework/dist/eventBus';

export function WhisperEvents() {
  Loop.createWhisper({
    components: {
      header: <Header>{},
      body: <Markdown>{},
      cta: <Button>{},
      select: <SelectFormField>{},
      radio: <RadioFormField>{},
      checkbox: <CheckboxFormField>{},
    },
    data: {
      myWhisper: <Whisper>{},
      state: {
        count: 0,
        select: -1,
        radio: -1,
        checkbox: false,
      },
    },
    methods: {
      getComponents(): WhisperComponent[] {
        this.header = Header({header: 'Inventore veritatis'});

        this.body = Markdown({
          body:
            'Click any of the elements below to see the counter in the whisper label change. The whisper is also updated while keeping state. Please note that performance has not been tested in relation to state and "whisper.update()".',
        });
        this.cta = Button({
          label: 'Click me',
        });

        this.select = SelectFormField({
          label: 'Select',
          options: ['1', '2'],
          selected: this.state.select,
        });

        this.radio = RadioFormField({
          options: ['1', '2'],
          selected: this.state.radio,
        });

        this.checkbox = CheckboxFormField({
          label: 'This is a chehckbox',
          value: this.state.checkbox,
        });

        this.addEventListeners();

        return [
          this.header,
          this.body,
          this.select,
          this.radio,
          this.checkbox,
          this.cta,
        ];
      },
      handleClickCTA(): void {
        console.warn('clicked');
        this.update();
      },
      handleSelectDropdown(val: number): void {
        console.log(`dropdown selected: ${val}`);
        this.state.select = val;
        this.update();
      },
      handleSelectRadio(val: number): void {
        console.log(`radio selected: ${val}`);
        this.state.radio = val;
        this.update();
      },
      handleChangeCheckbox(val: boolean): void {
        console.log(`checkbox changed: ${val}`);
        this.state.checkbox = val;
        this.update();
      },
      addEventListeners(): void {
        try {
          this.cta.on(EventType.Click, this.handleClickCTA.bind(this));
          this.select.on(
            EventType.Select,
            this.handleSelectDropdown.bind(this)
          );
          this.radio.on(EventType.Select, this.handleSelectRadio.bind(this));
          this.checkbox.on(
            EventType.Change,
            this.handleChangeCheckbox.bind(this)
          );
        } catch (e) {
          console.error(e);
        }
      },
      removeEventListeners(): void {
        this.cta.off(EventType.Click, this.handleClickCTA);
        this.select.off(EventType.Select, this.handleSelectDropdown);
        this.radio.off(EventType.Select, this.handleSelectRadio);
        this.checkbox.off(EventType.Change, this.handleChangeCheckbox);
        console.warn('Destroying event listeners');
      },
    },
    lifeCycle: {
      async render() {
        try {
          this.myWhisper = await whisper.create({
            label: `Whisper events updated (${this.state.count})`,
            components: renderComponents(...this.getComponents()),
            onClose: () => this.destroy(),
          });
        } catch (e) {
          console.log(e);
        }
      },
      update(): void {
        this.state.count++;

        this.myWhisper.update({
          label: `Whisper events (${this.state.count})`,
          components: renderComponents(...this.getComponents()),
        });
      },
      destroy(): void {
        this.removeEventListeners();
        console.warn('Destroying whisper');
      },
    },
  });
}
