/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {NumberFormField} from '../src/components';
import {
  ldkComponentProps,
  expectedRenderComponent,
  setComponentProps,
  testComponentProperties,
  TestMessage,
  testRenderWrappedLDKComponentToLDKComponent,
  // @ts-ignore
} from './helpers';

const {numberInput: defaultProps} = ldkComponentProps;

describe('numberInput component', () => {
  const component = NumberFormField(setComponentProps(defaultProps));

  it(TestMessage.CreatesWrappedLDKComponent, () => {
    testComponentProperties(component);
  });

  it(TestMessage.RendersWrappedLDKComponentToLDKComponent, () => {
    testRenderWrappedLDKComponentToLDKComponent(
      component,
      expectedRenderComponent(defaultProps)
    );
  });

  describe('sets properties', () => {
    describe('min', () => {
      it('is set to 0', () => {
        const component = NumberFormField(setComponentProps(defaultProps, {min: 0})).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {min: 0}));
      });
      it('is set to 1', () => {
        const component = NumberFormField(setComponentProps(defaultProps, {min: 1})).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {min: 1}));
      });
      it('is not set', () => {
        const component = NumberFormField(setComponentProps(defaultProps)).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps));
      });
    });

    describe('max', () => {
      it('is set to 0', () => {
        const component = NumberFormField(setComponentProps(defaultProps, {max: 0})).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {max: 0}));
      });
      it('is set to 1', () => {
        const component = NumberFormField(setComponentProps(defaultProps, {max: 1})).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {max: 1}));
      });
      it('is not set', () => {
        const component = NumberFormField(setComponentProps(defaultProps)).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps));
      });
    });

    describe('value', () => {
      it('is set to 0', () => {
        const component = NumberFormField(
          setComponentProps(defaultProps, {value: 0})
        ).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {value: 0}));
      });
      it('is set to 1', () => {
        const component = NumberFormField(
          setComponentProps(defaultProps, {value: 1})
        ).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {value: 1}));
      });
      it('is not set', () => {
        const component = NumberFormField(setComponentProps(defaultProps)).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps));
      });
    });

    describe('step', () => {
      it('is set to 0', () => {
        const component = NumberFormField(setComponentProps(defaultProps, {step: 0})).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {step: 0}));
      });
      it('is set to 0.1', () => {
        const component = NumberFormField(
          setComponentProps(defaultProps, {step: 0.1})
        ).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {step: 0.1}));
      });
      it('is set to 1', () => {
        const component = NumberFormField(setComponentProps(defaultProps, {step: 1})).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {step: 1}));
      });
      it('is not set', () => {
        const component = NumberFormField(setComponentProps(defaultProps)).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps));
      });
    });
  });
});
