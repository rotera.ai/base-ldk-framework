/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {WrappedLDKComponent} from '../src/utils';
import {
  Link,
  Button,
  TextFormField,
  NumberFormField,
  CheckboxFormField,
  PasswordFormField,
  SelectFormField,
  EmailFormField,
} from '../src/components';
// @ts-ignore
import {ldkComponentProps, setComponentProps} from './helpers';
import {events, EventType} from '../src/eventBus';

// Test that each component in fact has the event, can emit their
// events properly and that the on/off methods work.
// -----------------------------------------------------------------

const componentEventListeners: Record<string, Function> = {
  hasEvent: (eventType: EventType, listener: WrappedLDKComponent) => {
    it(`has event type ${eventType}`, () => {
      const onEventType = 'on' + eventType.charAt(0).toUpperCase() + eventType.slice(1);
      expect(listener.hasOwnProperty(onEventType)).toBeTruthy();
    });
  },
  onOff: (eventType: EventType, listener: WrappedLDKComponent) => {
    it('on', () => {
      const handler = jest.fn();
      listener.on(eventType, handler);
      listener.emit(eventType);
      listener.off(eventType, handler);
      expect(handler).toHaveBeenCalledTimes(1);
    });
  },
  onMultiple: (eventType: EventType, listener: WrappedLDKComponent) => {
    it('on multiple', () => {
      const handler = jest.fn();
      listener.on(eventType, handler);
      listener.emit(eventType);
      listener.emit(eventType);
      listener.off(eventType, handler);
      expect(handler).toHaveBeenCalledTimes(2);
    });
  },
  onOnce: (eventType: EventType, listener: WrappedLDKComponent) => {
    it('on once', () => {
      const handler = jest.fn();
      listener.on(eventType, handler);
      listener.emit(eventType);
      listener.off(eventType, handler);
      listener.emit(eventType);
      expect(handler).toHaveBeenCalledTimes(1);
    });
  },
};

const componentsThatEmitEvents = [
  {
    type: EventType.Click,
    components: [
      Link(setComponentProps(ldkComponentProps.link, {onClick: expect.any(Function)})),
      Button(setComponentProps(ldkComponentProps.button)),
    ],
  },
  {
    type: EventType.Blur,
    components: [
      TextFormField(setComponentProps(ldkComponentProps.textInput)),
      EmailFormField(setComponentProps(ldkComponentProps.email)),
      NumberFormField(setComponentProps(ldkComponentProps.numberInput)),
      PasswordFormField(setComponentProps(ldkComponentProps.password)),
    ],
  },
  {
    type: EventType.Focus,
    components: [
      TextFormField(setComponentProps(ldkComponentProps.textInput)),
      EmailFormField(setComponentProps(ldkComponentProps.email)),
      NumberFormField(setComponentProps(ldkComponentProps.numberInput)),
      PasswordFormField(setComponentProps(ldkComponentProps.password)),
    ],
  },
  {
    type: EventType.Change,
    components: [
      TextFormField(setComponentProps(ldkComponentProps.textInput)),
      EmailFormField(setComponentProps(ldkComponentProps.email)),
      NumberFormField(setComponentProps(ldkComponentProps.numberInput)),
      PasswordFormField(setComponentProps(ldkComponentProps.password)),
      CheckboxFormField(setComponentProps(ldkComponentProps.checkbox)),
    ],
  },
  {
    type: EventType.Select,
    components: [SelectFormField(setComponentProps(ldkComponentProps.select))],
  },
];

describe('component event listeners for', () => {
  componentsThatEmitEvents.forEach(emitter => {
    // Loop through each component that emits an event
    emitter.components.forEach(component => {
      // Group tests by component type
      describe(component.type, () => {
        // Loop though each event test scenario
        Object.keys(componentEventListeners).forEach(event => {
          // Group tests by the event type
          describe(emitter.type, () => {
            // Call each test from `events` object
            componentEventListeners[event](emitter.type, component);
          });
        });
      });
    });
  });
});

// Test event handlers
// -----------------------------------------------------------------

const testOnClick = Link({text: 'abc'});
const testOnBlurOnFocusOnChange = TextFormField({label: 'abc'});
const testOnSelect = SelectFormField({label: 'abc', options: ['1', '2']});

const testWhisper = {
  id: '',
  close: expect.any(Function),
  update: expect.any(Function),
  componentState: new Map(),
};
const testValue = 'some value';
const eventHandler = jest.fn();

describe('event handlers', () => {
  const eventHandlers = [
    {handler: events.onClick(testOnClick), type: 'onClick'},
    {handler: events.onBlur(testOnBlurOnFocusOnChange), type: 'onBlur'},
    {handler: events.onFocus(testOnBlurOnFocusOnChange), type: 'onFocus'},
  ];

  const eventHandlersWithValue = [
    {handler: events.onChangeWithValue(testOnBlurOnFocusOnChange), type: 'onChangeWithValue'},
    {handler: events.onSelectWithValue(testOnSelect), type: 'onSelectWithValue'},
  ];

  describe('can throw errors', () => {
    eventHandlers.forEach(({handler, type}) => {
      it(`throws error for ${type}`, () => {
        expect(() => {
          handler(new Error('This is an error.'), testWhisper);
        }).toThrow('This is an error.');
      });
    });

    eventHandlersWithValue.forEach(({handler, type}) => {
      it(`throws error for ${type}`, () => {
        expect(() => {
          handler(new Error('This is an error.'), testValue, testWhisper);
        }).toThrow('This is an error.');
      });
    });
  });

  describe('emits events', () => {
    beforeEach(() => {
      testOnClick.on(EventType.Click, eventHandler);
      testOnBlurOnFocusOnChange.on(EventType.Blur, eventHandler);
      testOnBlurOnFocusOnChange.on(EventType.Focus, eventHandler);
      testOnBlurOnFocusOnChange.on(EventType.Change, eventHandler);
      testOnSelect.on(EventType.Select, eventHandler);
    });
    afterEach(() => {
      jest.clearAllMocks();
      testOnClick.off(EventType.Click, eventHandler);
      testOnBlurOnFocusOnChange.off(EventType.Blur, eventHandler);
      testOnBlurOnFocusOnChange.off(EventType.Focus, eventHandler);
      testOnBlurOnFocusOnChange.off(EventType.Change, eventHandler);
      testOnSelect.off(EventType.Select, eventHandler);
    });

    it('can emit relative events with NO arguments', () => {
      eventHandlers.forEach(({handler}) => {
        handler(undefined, testWhisper);
      });
      expect(eventHandler).toBeCalledTimes(eventHandlers.length);
      expect(eventHandler).toBeCalledWith();
    });

    it('can emit relative events with arguments', () => {
      eventHandlersWithValue.forEach(({handler}) => {
        handler(undefined, testValue, testWhisper);
      });
      expect(eventHandler).toBeCalledTimes(eventHandlersWithValue.length);
      expect(eventHandler).toBeCalledWith(testValue, testWhisper);
    });
  });
});
