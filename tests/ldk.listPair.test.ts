/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {ListPair} from '../src/components';
import {
  testComponentProperties,
  testRenderWrappedLDKComponentToLDKComponent,
  setComponentProps,
  ldkComponentProps,
  TestMessage,
  expectedRenderComponent,
  // @ts-ignore
} from './helpers';

const {listPair: defaultProps} = ldkComponentProps;

describe('list pair component', () => {
  const component = ListPair(setComponentProps(defaultProps));

  it(TestMessage.CreatesWrappedLDKComponent, () => {
    testComponentProperties(component);
  });

  it(TestMessage.RendersWrappedLDKComponentToLDKComponent, () => {
    testRenderWrappedLDKComponentToLDKComponent(
      component,
      expectedRenderComponent(defaultProps)
    );
  });
});
