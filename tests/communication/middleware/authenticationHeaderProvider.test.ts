/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {AuthenticationHeaderHttpProvider} from '../../../src/communication/middleware/authenticationHeaderHttpProvider';
import {fetchSimpleAjaxGetRequest, fetchSimpleAjaxPostRequest} from '../../../src/communication/api/simpleApiClients';
import {HttpMethods} from "../../../src/communication/api/http";

jest.mock('../../../src/communication/api/simpleApiClients');

const mockAuthenticationProvider = {
  canRetry: jest.fn(),
  getAuthenticationHeader: jest.fn(),
};

describe('src/communitcation/middleware/AuthenticationHeaderHttpProvider', () => {
  const classToTest = new AuthenticationHeaderHttpProvider(mockAuthenticationProvider);
  describe('AuthenticationHeaderProvider class', () => {
    describe('httpRequest', () => {
      it.todo('');
    });
    describe('createHeaders', () => {
      it('should return the string provided by get authtication header without additional headers', async () => {
        const headers = {};
        const mockHeader = 'this is a header';
        mockAuthenticationProvider.getAuthenticationHeader.mockImplementationOnce((retry: boolean) => {
          return {
            authHeaderName: 'token',
            authHeaderValue: [mockHeader],
          };
        });
        const expected = {
          token: [mockHeader]
        };
        const actual = await classToTest.createHeaders(headers, true);
        expect(actual).toEqual(expected);
      });
    });
    describe('getHttpFunction', () => {
      it('should return the get function', () => {
        const httpMethod = HttpMethods.GET;
        const actual = classToTest.getHttpFunction(httpMethod);
        expect(actual).toEqual(fetchSimpleAjaxGetRequest);
      });
      it('should return the post function', () => {
        const httpMethod = HttpMethods.POST;
        const actual = classToTest.getHttpFunction(httpMethod);
        expect(actual).toEqual(fetchSimpleAjaxPostRequest);
      });
    });
  });
});
