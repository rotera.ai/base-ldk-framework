
import {MultipartFormData} from "../../../src/communication/api/multipartFormData"
import {system} from "@oliveai/ldk";
const multipart = require('parse-multipart-data');
const Buffer = require('buffer/').Buffer;

let operatingSystemSpy: jest.SpyInstance;

describe('communications/api/multipartFormData', () => {
  beforeEach(() => {
    operatingSystemSpy = jest.spyOn(system, 'operatingSystem').mockImplementation().mockClear();
  });
  it('Create form. test fields coming out', async () => {
    operatingSystemSpy.mockReturnValue('posix');

    const sourceFormData = new MultipartFormData();
    await sourceFormData.append("foo", "bar");
    await sourceFormData.append("bletch", "spam");
    const octetData = Buffer.from([1,2,3,4,5,6,9,8,8]);
    await sourceFormData.append("yada", octetData, {
      filename: "foo.pdf"
    });

    const encodedSourceData = sourceFormData.getBuffer();

    const parsedData = multipart.parse(encodedSourceData, sourceFormData.getBoundary())

    expect(parsedData[0].name).toStrictEqual("foo");
    expect(parsedData[0].data.toString()).toStrictEqual("bar");
    expect(parsedData[1].name).toStrictEqual("bletch");
    expect(parsedData[1].data.toString()).toStrictEqual("spam");

    // The parser does not keep the field name.
    expect(parsedData[2].filename).toStrictEqual("foo.pdf");

    // ParsedData has a real buffer, as opposed to the library one used by the multipart library.
    parsedData[2].data.valueOf().forEach((value: number, index: number) => {
      expect(value).toStrictEqual(octetData.valueOf()[index]);
    })
  });
});
