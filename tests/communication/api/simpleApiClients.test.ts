import {network, system} from '@oliveai/ldk';
import {
  buildQueryString, fetchSimpleAjaxFormPostRequest,
  fetchSimpleAjaxGetRequest,
  fetchSimpleAjaxPostRequest
} from "../../../src/communication/api/simpleApiClients";

import {MultipartFormData} from "../../../src/communication/api/multipartFormData"
const Buffer = require('buffer/').Buffer;

let httpRequestSpy: jest.SpyInstance;
let decodeSpy: jest.SpyInstance;
let encodeSpy: jest.SpyInstance;

let operatingSystemSpy: jest.SpyInstance;

type TestRequest = {
  foo: string
  bar: number
}

type TestResponse = {
  bletch: string
  spam: number
}

describe('communications/api/simpleApiClients', () => {
  beforeEach(() => {
    operatingSystemSpy = jest.spyOn(system, 'operatingSystem').mockImplementation().mockClear();
    httpRequestSpy = jest.spyOn(network, 'httpRequest').mockImplementation().mockClear();
    decodeSpy = jest.spyOn(network, 'decode').mockImplementation().mockClear();
    encodeSpy = jest.spyOn(network, 'encode').mockImplementation().mockClear();
  });
  it('GET, successful, no JSON request and has JSON response', async () => {

    const testResponse: TestResponse = {
      bletch: 'foop',
      spam: 12345
    };
    httpRequestSpy.mockReturnValueOnce({body: {}, statusCode: 200});
    decodeSpy.mockReturnValueOnce(JSON.stringify(testResponse));

    const url = "https://api.foo.bar";

    expect(await fetchSimpleAjaxGetRequest(url)).toStrictEqual(testResponse);

    // Use buildQueryString since we don't know what order the object keys are iterated through.
    expect(httpRequestSpy.mock.calls[0][0]).toStrictEqual({
      url: url,
      method: 'GET',
      headers: undefined
    });
  });
  it('GET, successful, has JSON request and has JSON response', async () => {

    const testRequest: TestRequest = {
      foo: "a=b",
      bar: 4321
    }

    const testResponse: TestResponse = {
      bletch: 'foop',
      spam: 12345
    };

    httpRequestSpy.mockReturnValueOnce({body: {}, statusCode: 200});
    decodeSpy.mockReturnValueOnce(JSON.stringify(testResponse));

    const url = "https://api.foo.bar";

    expect(await fetchSimpleAjaxGetRequest(url, testRequest)).toStrictEqual(testResponse);

    // Use buildQueryString since we don't know what order the object keys are iterated through.
    expect(httpRequestSpy.mock.calls[0][0]).toStrictEqual({
      url: url + buildQueryString(testRequest),
      method: 'GET',
      headers: undefined
    });
  });
  it('POST, successful, has JSON request and has JSON response', async () => {

    const testRequest: TestRequest = {
      foo: "a=b",
      bar: 4321
    }

    const testResponse: TestResponse = {
      bletch: 'foop',
      spam: 12345
    };

    const encodedRequest = [0,1,2,3,4,5,6,7];
    const encodedResponse = [9,8,7,6,5,4,3,2,1];
    httpRequestSpy.mockReturnValueOnce({body: encodedResponse, statusCode: 200});
    encodeSpy.mockReturnValueOnce(encodedRequest);
    decodeSpy.mockReturnValueOnce(JSON.stringify(testResponse));

    const url = "https://api.foo.bar";

    expect(await fetchSimpleAjaxPostRequest(url, testRequest)).toStrictEqual(testResponse);

    expect(httpRequestSpy.mock.calls[0][0]).toStrictEqual({
      url: url,
      method: 'POST',
      headers: {
        'content-type': ['application/json']
      },
      body: encodedRequest
    });
    expect(encodeSpy.mock.calls[0][0]).toBe(JSON.stringify(testRequest));
    expect(decodeSpy.mock.calls[0][0]).toBe(encodedResponse);
  });
  it('form POST, successful, has form request and has JSON response', async () => {
    operatingSystemSpy.mockReturnValue('posix');

    const formData = new MultipartFormData();
    const boundary = '----foop'
    formData.setBoundary(boundary)
    await formData.append("foo", "bar");
    await formData.append("bar", "bletch");
    await formData.append("glorp", "yadaya", {
      filename: "foo.txt"
    })
    await formData.append("yada", Buffer.from([65, 66, 67, 68, 69, 70]));

    const testResponse: TestResponse = {
      bletch: 'foop',
      spam: 12345
    };

    const encodedResponse = [9,8,7,6,5,4,3,2,1];
    httpRequestSpy.mockReturnValueOnce({body: encodedResponse, statusCode: 200});
    decodeSpy.mockReturnValueOnce(JSON.stringify(testResponse));

    const url = "https://api.foo.bar";

    expect(await fetchSimpleAjaxFormPostRequest(url, formData)).toStrictEqual(testResponse);

    expect(httpRequestSpy.mock.calls[0][0]).toStrictEqual({
      url: url,
      method: 'POST',
      headers: {
        'content-type': [`multipart/form-data; boundary=${boundary}`]
      },
      body: formData.getBuffer().valueOf()
    });
  });
});
