/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Divider} from '../src/components';
import {whisper} from '@oliveai/ldk';

it('divider component', () => {
  expect(Divider()).toEqual({
    type: whisper.WhisperComponentType.Divider,
  });
});
