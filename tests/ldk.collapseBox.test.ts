/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import { CollapseBox } from '../src/components';
import {
    testComponentProperties,
    testRenderWrappedLDKComponentToLDKComponent,
    setComponentProps,
    expectedRenderComponent,
    ldkComponentProps,
    TestMessage,
} from './helpers';

const { collapseBox: defaultProps } = ldkComponentProps;

describe('collapseBox component', () => {
    const component = CollapseBox(setComponentProps(defaultProps, { label: 'Test Collapse Box' }));

    it(TestMessage.CreatesWrappedLDKComponent, () => {
        testComponentProperties(component);
    });

    it(TestMessage.RendersWrappedLDKComponentToLDKComponent, () => {
        testRenderWrappedLDKComponentToLDKComponent(
            component,
            expectedRenderComponent(defaultProps, { label: 'Test Collapse Box' })
        );
    });
});
