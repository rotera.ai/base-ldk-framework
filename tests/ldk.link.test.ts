/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Link} from '../src/components';
import {
  testComponentProperties,
  testRenderWrappedLDKComponentToLDKComponent,
  url,
  setComponentProps,
  expectedRenderComponent,
  ldkComponentProps,
  TestMessage,
  // @ts-ignore
} from './helpers';

const {link: defaultProps} = ldkComponentProps;

describe('link component', () => {
  const component = Link(setComponentProps(defaultProps, {href: url}));

  it(TestMessage.CreatesWrappedLDKComponent, () => {
    testComponentProperties(component);
  });

  it(TestMessage.RendersWrappedLDKComponentToLDKComponent, () => {
    testRenderWrappedLDKComponentToLDKComponent(
      component,
      expectedRenderComponent(defaultProps, {href: url})
    );
  });

  describe('sets properties', () => {
    // Creates link with either `href` or `onClick`
    describe('action', () => {
      it('href', () => {
        const component = Link(setComponentProps(defaultProps, {href: url})).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {href: url}));
      });
      it('onClick', () => {
        const component = Link(setComponentProps(defaultProps)).render();
        expect(component).toEqual(
          expectedRenderComponent(defaultProps, {onClick: expect.any(Function)})
        );
      });
    });
  });
});
