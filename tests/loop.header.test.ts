/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Header} from '../src/components';
import {
  ldkComponentProps,
  expectedRenderComponent,
  setComponentProps,
  testComponentProperties,
  TestMessage,
  testRenderWrappedLDKComponentToLDKComponent,
  // @ts-ignore
} from './helpers';

const {header: defaultProps} = ldkComponentProps;

describe('header component', () => {
  const component = Header(setComponentProps({header: 'header'}));

  it(TestMessage.CreatesWrappedLDKComponent, () => {
    testComponentProperties(component);
  });

  it(TestMessage.RendersWrappedLDKComponentToLDKComponent, () => {
    testRenderWrappedLDKComponentToLDKComponent(
      component,
      expectedRenderComponent(defaultProps)
    );
  });
});
