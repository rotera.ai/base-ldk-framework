/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {RadioFormField} from '../src/components';
import {
  testComponentProperties,
  testRenderWrappedLDKComponentToLDKComponent,
  url,
  setComponentProps,
  expectedRenderComponent,
  ldkComponentProps,
  TestMessage,
  // @ts-ignore
} from './helpers';

const {radioGroup: defaultProps} = ldkComponentProps;

describe('radioGroup component', () => {
  const component = RadioFormField(setComponentProps(defaultProps, {href: url}));

  it(TestMessage.CreatesWrappedLDKComponent, () => {
    testComponentProperties(component);
  });

  it(TestMessage.RendersWrappedLDKComponentToLDKComponent, () => {
    testRenderWrappedLDKComponentToLDKComponent(
      component,
      expectedRenderComponent(defaultProps)
    );
  });

  describe('sets properties', () => {
    describe('selected', () => {
      it('is set to 0', () => {
        const component = RadioFormField(
          setComponentProps(defaultProps, {selected: 0})
        ).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {selected: 0}));
      });
      it('is set to 1', () => {
        const component = RadioFormField(
          setComponentProps(defaultProps, {selected: 1})
        ).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps, {selected: 1}));
      });
      it('is not set', () => {
        const component = RadioFormField(setComponentProps(defaultProps)).render();
        expect(component).toEqual(expectedRenderComponent(defaultProps));
      });
    });
  });
});
