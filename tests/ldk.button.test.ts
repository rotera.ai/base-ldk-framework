/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Button} from '../src/components';

import {
  testComponentProperties,
  testRenderWrappedLDKComponentToLDKComponent,
  ldkComponentProps,
  setComponentProps,
  expectedRenderComponent,
  TestMessage,
  // @ts-ignore
} from './helpers';

const {button: defaultProps} = ldkComponentProps;

describe('button component', () => {
  const component = Button(setComponentProps(defaultProps));

  it(TestMessage.CreatesWrappedLDKComponent, () => {
    testComponentProperties(component);
  });

  it(TestMessage.RendersWrappedLDKComponentToLDKComponent, () => {
    testRenderWrappedLDKComponentToLDKComponent(
      component,
      expectedRenderComponent(defaultProps)
    );
  });
});
