/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {Header, Button, Link} from '../src/components';
import {Loop, WhisperComponent, renderComponents, LDKComponent, hasOwnProperty} from '../src/utils';
import {Component} from '@oliveai/ldk/dist/whisper';

describe('utils', () => {
  // Render components
  // -----------------------------------------------------------------

  it('should render components', () => {
    const components = [
      Header({header: 'abc'}).render(),
      Button({label: 'def'}).render(),
      {
        text: 'ghi',
        href: 'https://google.com',
        style: whisper.Urgency.None,
        type: whisper.WhisperComponentType.Link,
      },
    ];

    const actual = renderComponents(components);

    const expected = [
      [{body: '# abc', type: 'markdown'}],
      [
        {
          label: 'def',
          buttonStyle: 'primary',
          size: 'large',
          tooltip: '',
          onClick: expect.any(Function),
          type: 'button',
        },
      ],
      {
        text: 'ghi',
        href: 'https://google.com',
        style: 'none',
        type: 'link',
      },
    ];

    expect(expected).toEqual(actual);
  });

  // Create component
  // -----------------------------------------------------------------

  it('should create component', () => {
    const actual = Loop.createComponent({
      type: 'test',
      props: {
        prop1: 'prop 1',
      },
      data: {
        data1: 'data 1',
      },
      methods: {
        // Example of single LDK `Component`
        getComponent1(): Component {
          return {
            body: 'a',
            type: whisper.WhisperComponentType.Markdown,
          };
        },
        // Example of array of LDK `Component`
        getComponent2(): LDKComponent[] {
          return [
            {
              body: 'a',
              type: whisper.WhisperComponentType.Markdown,
            },
            {
              body: 'b',
              type: whisper.WhisperComponentType.Markdown,
            },
          ];
        },
        // Example of array of `Component` using our `WhisperComponent[]` type. We can use either option.
        getComponent3(): WhisperComponent[] {
          return [
            Link({text: 'abc'}),
            {
              body: 'a',
              type: whisper.WhisperComponentType.Markdown,
            },
            {
              body: 'b',
              type: whisper.WhisperComponentType.Markdown,
            },
          ];
        },
        getComponents(): WhisperComponent[] {
          return [
            Link({text: 'abc'}),
            {
              body: 'a',
              type: whisper.WhisperComponentType.Markdown,
            },
            this.getComponent1(),
            ...this.getComponent2(),
            ...this.getComponent3(),
          ];
        },
      },
      lifeCycle: {
        render(): LDKComponent[] {
          return renderComponents(...this.getComponents());
        },
      },
    });

    const expected = {
      type: 'test',
      prop1: 'prop 1',
      data1: 'data 1',
      getComponent1: expect.any(Function),
      getComponent2: expect.any(Function),
      getComponent3: expect.any(Function),
      getComponents: expect.any(Function),
      render: expect.any(Function),
      emit: expect.any(Function),
      on: expect.any(Function),
      off: expect.any(Function),
    };

    expect(actual).toStrictEqual(expected);
  });

  // Create whisper
  // -----------------------------------------------------------------

  it('should create whisper and returns void', () => {
    const actual = Loop.createWhisper({
      data: {
        testData: 'some data',
      },
      methods: {
        getData() {
          console.log('where requests to data goes');
        },
        getComponent1(): Component {
          return {
            body: 'a',
            type: whisper.WhisperComponentType.Markdown,
          };
        },
        getComponent2(): LDKComponent[] {
          return [
            {
              body: 'a',
              type: whisper.WhisperComponentType.Markdown,
            },
            {
              body: 'b',
              type: whisper.WhisperComponentType.Markdown,
            },
          ];
        },
        getComponent3(): WhisperComponent[] {
          return [
            {
              body: 'a',
              type: whisper.WhisperComponentType.Markdown,
            },
            {
              body: 'b',
              type: whisper.WhisperComponentType.Markdown,
            },
          ];
        },
        getComponents(): WhisperComponent[] {
          return [
            {
              body: 'a',
              type: whisper.WhisperComponentType.Markdown,
            },
            this.getComponent1(),
            ...this.getComponent2(),
            ...this.getComponent3(),
          ];
        },
      },
      lifeCycle: {
        render(): void {
          // Note that a real-world example would include `whisper.create()`
          renderComponents(...this.getComponents());
        },
      },
    });

    expect(actual).toBeUndefined();
  });
  describe('hasOwnProperty', () => {
    it('should return true if object has given field', () => {
      const testObject = {
        key: 'anything'
      };
      const actual = hasOwnProperty(testObject, 'key');
      expect(actual).toBeTruthy();
    });
    it('should return false if object does not have given field', () => {
      const testObject = {
        key: 'anything'
      };
      const actual = hasOwnProperty(testObject, 'does not exist');
      expect(actual).toBeFalsy();
    });
  });
});
