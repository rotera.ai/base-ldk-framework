/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import { WrappedLDKComponent, LDKComponent } from '../src/utils';
import { whisper } from '@oliveai/ldk';

/**
 * Global static content we reuse throughout tests.
 */
export const url = 'https://www.rotera.ai/';

/**
 * Wrapped component public properties
 *
 * The publicly accessible properties from a wrapped component
 */
export const wrappedComponentPublicProperties = {
  render: expect.any(Function),
  emit: expect.any(Function),
  on: expect.any(Function),
  off: expect.any(Function),
};

/**
 * Component props
 *
 * The default properties for each component type that we'll use with testing component creation.
 */
export const ldkComponentProps = {
  link: {
    text: 'link',
    style: whisper.Urgency.None,
    textAlign: whisper.TextAlign.Left,
    type: whisper.WhisperComponentType.Link,
  },
  button: {
    label: 'button',
    buttonStyle: whisper.ButtonStyle.Primary,
    size: whisper.ButtonSize.Large,
    tooltip: '',
    onClick: expect.any(Function),
    type: whisper.WhisperComponentType.Button,
  },
  listPair: {
    label: 'listPair',
    value: 'abc',
    labelCopyable: false,
    copyable: false,
    style: whisper.Urgency.None,
    type: whisper.WhisperComponentType.ListPair,
  },
  message: {
    header: 'message',
    body: 'abc',
    style: whisper.Urgency.None,
    textAlign: whisper.TextAlign.Left,
    type: whisper.WhisperComponentType.Message,
  },
  checkbox: {
    label: 'checkboxFormField',
    tooltip: 'abc',
    value: false,
    onChange: expect.any(Function),
    type: whisper.WhisperComponentType.Checkbox,
  },
  textInput: {
    label: 'textFormField',
    tooltip: 'abc',
    value: '',
    onChange: expect.any(Function),
    onBlur: expect.any(Function),
    onFocus: expect.any(Function),
    type: whisper.WhisperComponentType.TextInput,
  },
  password: {
    label: 'passwordFormField',
    tooltip: 'abc',
    value: '',
    onChange: expect.any(Function),
    onBlur: expect.any(Function),
    onFocus: expect.any(Function),
    type: whisper.WhisperComponentType.Password,
  },
  numberInput: {
    label: 'numberFormField',
    tooltip: 'abc',
    onChange: expect.any(Function),
    onBlur: expect.any(Function),
    onFocus: expect.any(Function),
    type: whisper.WhisperComponentType.Number,
  },
  select: {
    label: 'selectFormField',
    tooltip: 'abc',
    options: ['1', '2'],
    selected: 1,
    onSelect: expect.any(Function),
    type: whisper.WhisperComponentType.Select,
  },
  email: {
    label: 'emailFormField',
    tooltip: 'abc',
    value: '',
    onChange: expect.any(Function),
    onBlur: expect.any(Function),
    onFocus: expect.any(Function),
    type: whisper.WhisperComponentType.Email,
  },
  radioGroup: {
    options: ['1', '2'],
    onSelect: expect.any(Function),
    type: whisper.WhisperComponentType.RadioGroup,
  },
  telephone: {
    label: 'emailFormField',
    tooltip: 'abc',
    value: '',
    onChange: expect.any(Function),
    onBlur: expect.any(Function),
    onFocus: expect.any(Function),
    type: whisper.WhisperComponentType.Telephone,
  },
  markdown: {
    body: '# markdown',
    type: whisper.WhisperComponentType.Markdown,
  },
  header: {
    body: '# header',
    type: whisper.WhisperComponentType.Markdown,
  },
  handlebars: {
    body: '# handlebars',
    type: whisper.WhisperComponentType.Markdown,
  },
  collapseBox: {
    label: '',
  }
};

/**
 * Set component props
 *
 * The method we use to set a component's props in various tests.
 */
export function setComponentProps(defaultProps: Record<string, any>, args?: any | {}) {
  return {
    ...defaultProps,
    ...args,
  };
}

/**
 * Expected component
 *
 * What the component should look like once rendered.
 */
export function expectedRenderComponent(
  defaultProps: Record<string, any>,
  args?: any | {}
): LDKComponent[] {
  return [
    {
      ...defaultProps,
      ...args,
    },
  ];
}

/**
 * Test Message
 *
 * Global messages we use throughout tests.
 */
export const TestMessage = {
  CreatesWrappedLDKComponent: `creates wrapped component & has public properties: ${Object.keys(
    wrappedComponentPublicProperties
  )}`,
  RendersWrappedLDKComponentToLDKComponent: 'renders wrapped component to LDK component',
};

/**
 * Test component properties
 *
 */
export function testComponentProperties(component: WrappedLDKComponent) {
  expect(typeof component === 'object').toBe(true);
  // TODO: Test each `WrappedLDKComponent` base properties. Add new function param for matching the props.
  // Ensure the wrapped components have public properties like `on`, `off`, `emit`, `render`
  Object.keys(wrappedComponentPublicProperties).forEach(key => {
    expect(component.hasOwnProperty(key)).toBe(true);
    expect(typeof component[key as keyof WrappedLDKComponent] === 'function').toBe(true);
  });
}

/**
 * Test render wrapped component to LDK component
 *
 * Ensure that when `render()` is called from the wrapped component it returns
 * the correct properties the LDK component expects.
 */
export async function testRenderWrappedLDKComponentToLDKComponent(
  wrappedComponent: WrappedLDKComponent,
  expected: LDKComponent[]
) {
  const rendered = wrappedComponent.render();
  expect(Array.isArray(rendered)).toBe(true);
  expect(rendered).toEqual(expected);
}
